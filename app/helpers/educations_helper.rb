module EducationsHelper
  def schools_options
    @_schools_options ||= begin
      Organization.school.map { |school| [display_name(school), school.id] }
    end
  end

  def degrees_options
    @_degrees_options ||= begin
      EducationDegree.all.map { |c| [display_name(c), c.id] }
    end
  end

  def courses_options
    @_courses_options ||= begin
      EducationCourse.all.map { |c| [display_name(c), c.id] }
    end
  end
end
