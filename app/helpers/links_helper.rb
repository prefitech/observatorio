module LinksHelper
  def predefined_labels_options
    ["Facebook",
     "LinkedIn",
     "Wikipedia",
     Link.human_attribute_name(:label_webpage)].sort +
      [[Link.human_attribute_name(:label_other), nil]]
  end
end
