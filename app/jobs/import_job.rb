class ImportJob < ApplicationJob
  ImportError = Class.new(StandardError)

  class ProcessLineError < StandardError
    attr_reader :line_number

    def initialize(line_number, error)
      super(error.message)
      @line_number = line_number
    end
  end

  class ProcessError < StandardError
    attr_reader :errors

    def initialize(errors)
      @errors = errors
    end
  end

  queue_as :default
  around_perform :track_status

  def perform(id)
    @import = Import.find(id)
    user = @import.user
    if user.nil?
      raise ProcessError.new([{message: I18n.t("errors.import.user_deleted")}])
    end
    PaperTrail.request.whodunnit = user.id
    preimport_errors = preimport_checks
    raise ProcessError.new(preimport_errors) if preimport_errors.present?
    import_errors = perform_import
    raise ProcessError.new(import_errors) if import_errors.present?
  end

  private

  def track_status
    import = Import.find(arguments[0])
    return unless import.status == "pending"
    import.update_attributes!(status: "processing")
    begin
      ActiveRecord::Base.transaction do
        yield
        import.update_attributes!(status: "success")
      end
    rescue ProcessError => e
      ActiveRecord::Base.transaction do
        e.errors.each do |error|
          import.log_error(error[:message], error[:line_number])
        end
        import.update_attributes!(status: "error")
      end
    rescue ProcessLineError => e
      ActiveRecord::Base.transaction do
        import.log_error(e.message, e.line_number)
        import.update_attributes!(status: "error")
      end
    rescue => e
      ActiveRecord::Base.transaction do
        import.log_error(e.message)
        import.update_attributes!(status: "error")
      end
    end
  end

  def preimport_checks
    raise NotImplementedError
  end

  def perform_import
    raise NotImplementedError
  end
end
