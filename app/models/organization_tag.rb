class OrganizationTag < Tag
  has_many :tagged_organizations, foreign_key: "tag_id"
  has_many :organizations, through: :tagged_organizations

  validates_uniqueness_of :name, case_sensitive: false
end
