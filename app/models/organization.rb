class Organization < ApplicationRecord
  scope :school, -> {
    where(school: true)
  }

  default_value :school, false

  has_paper_trail
  has_links
  has_attachments
  has_notes
  has_picture
  has_location :headquarters
  has_date_boundaries from_date: :date_of_foundation, to_date: :date_of_extinction
  has_names

  strip :abbreviation

  has_many :members, {
    class_name: "OrganizationMember",
    inverse_of: :organization,
    foreign_key: "organization_id",
    dependent: :destroy
  }
  has_many :owners, {
    class_name: "OrganizationOwner",
    inverse_of: :organization,
    foreign_key: "organization_id",
    dependent: :destroy
  }
  has_many :organizations_owner, {
    class_name: "OrganizationOwner",
    inverse_of: :owner,
    as: :owner,
    foreign_key: "owner_id",
    dependent: :destroy
  }
  has_many :events_participant, {
    class_name: "EventParticipant",
    inverse_of: :participant,
    as: :participant,
    foreign_key: "participant_id",
    dependent: :destroy
  }
  has_many :departments, {
    class_name: "OrganizationDepartment",
    inverse_of: :organization,
    foreign_key: "organization_id",
    dependent: :destroy
  }
  has_many :students, {
    class_name: "Education",
    inverse_of: :school,
    foreign_key: "school_id",
    dependent: :destroy
  }
  has_many :tagged_organizations, {
    class_name: "TaggedOrganization",
    inverse_of: :organization,
    foreign_key: "organization_id"
  }
  has_many :tags, through: :tagged_organizations, dependent: :destroy

  validates :cnpj, uniqueness: true, cnpj: true, if: :cnpj?
  validate :consistent_educations

  before_validation :normalize_cnpj

  def formatted_cnpj
    return nil if cnpj.nil?
    "#{cnpj[0..1]}.#{cnpj[2..4]}.#{cnpj[5..7]}/#{cnpj[8..11]}-#{cnpj[12..13]}"
  end

  private

  def consistent_educations
    return if school? || students.empty?
    errors.add(:school, errors.generate_message(:school, :inconsistent_educations))
  end

  def normalize_cnpj
    return if cnpj.nil?
    self.cnpj = cnpj.gsub(/\D/, "")
  end
end
