class OrganizationsImport < Import
  private

  def mandatory_columns
    %i(name)
  end
end
