class OrganizationMember < ApplicationRecord
  self.table_name = "organizations_members"

  has_paper_trail
  has_links :sources
  has_attachments
  has_date_boundaries
  has_immutable_attributes :organization_id, :member_id

  belongs_to :member,      class_name: "Person"
  belongs_to :organization
  belongs_to :position,    class_name: "OrganizationPosition"
  belongs_to :department,  class_name: "OrganizationDepartment"

  has_location

  validates_presence_of :member
  validates_presence_of :organization
  validate              :validate_department
  validate              :validate_duplicate

  private

  def validate_department
    return if organization.nil? ||
                department.nil? ||
                department.organization == organization
    errors.add(:department_id, errors.generate_message(:department, :doesnt_belong_to_organization))
  end

  def validate_duplicate
    return unless organization.present? && member.present?
    return if self.class
                  .where(":id IS NULL OR id != :id", id: id)
                  .where(organization: organization)
                  .where(member: member)
                  .where(position: position)
                  .where(department: department)
                  .where(location: location)
                  .where("from_date <= :to_date OR " \
                         ":to_date IS NULL OR " \
                         "from_date IS NULL",
                         to_date: to_date)
                  .where("to_date >= :from_date OR " \
                         ":from_date IS NULL OR " \
                         "to_date IS NULL",
                         from_date: from_date)
                  .empty?
    errors.add(:position_id, I18n.t("errors.messages.taken"))
    errors.add(:organization_id, I18n.t("errors.messages.taken"))
    errors.add(:member_id, I18n.t("errors.messages.taken"))
  end
end
