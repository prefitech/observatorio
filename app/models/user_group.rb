class UserGroup < ApplicationRecord
  has_name_and_display_priority
  default_value :permissions, {}

  validates_uniqueness_of :name

  has_and_belongs_to_many :users
end
