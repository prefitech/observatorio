class EventTag < Tag
  has_many :tagged_events, foreign_key: "tag_id"
  has_many :events, through: :tagged_events

  validates_uniqueness_of :name, case_sensitive: false
end
