class User < ApplicationRecord
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :validatable
  
  has_time_zone

  has_many :notes, {
    inverse_of: :author,
    foreign_key: "author_id",
    dependent: :destroy
  }
  has_and_belongs_to_many :groups, class_name: "UserGroup", join_table: :user_groups_users

  def display_name
    email
  end
end
