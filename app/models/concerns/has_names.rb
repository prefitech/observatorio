module HasNames
  extend ActiveSupport::Concern

  class_methods do
    def has_names
      scope :search, lambda { |query|
        joins(:names)
          .where(
            "UNACCENT(names.value) ILIKE UNACCENT(?)",
            "%#{query.split.join("%")}%"
          )
      }

      scope :order_by_main_name, -> {
        joins("LEFT OUTER JOIN names AS main_names
                 ON main_names.nameable_id = #{klass.table_name}.id AND
                    main_names.nameable_type = '#{klass}'")
          .where("main_names.main = ?", true)
          .order("main_names.value")
      }

      has_many :names, {
        as: :nameable,
        inverse_of: :nameable,
        dependent: :destroy
      }
      has_one :main_name, -> { where(main: true) }, {
        class_name: "Name",
        as: :nameable,
        inverse_of: :nameable,
        validate: true,
        autosave: true,
        dependent: :destroy
      }
      has_many :alternative_names, -> { where(main: false) }, {
        class_name: "Name",
        as: :nameable,
        inverse_of: :nameable,
        dependent: :destroy
      }

      validates_presence_of :main_name
      validate :main_name_is_main

      define_method :"name=" do |value|
        if main_name.present?
          main_name.value = value
        else
          self.main_name = Name.new(
            main: true,
            value: value,
            nameable: self
          )
        end
      end

      define_method :name do
        main_name && main_name.value
      end

      define_method :main_name_is_main do
        return unless main_name.present? && !main_name.main
        errors.add(:main_name, "not.valid")
      end
    end

    def find_by_main_name(value)
      joins(:main_name)
        .where("names.value = ?", value)
        .first
    end
  end
end
