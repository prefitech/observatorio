module HasNameAndDisplayPriority
  extend ActiveSupport::Concern

  class_methods do
    def has_name_and_display_priority(options = {})
      default_scope { order(:display_priority, :name) }

      scope :search, lambda { |query|
        where("unaccent(name || name) ilike unaccent(?)", "%#{query.split.join("%")}%")
      }

      validates_presence_of :display_priority
      validates_presence_of :name

      strip                 :name
      default_value         :display_priority, 0
    end
  end
end
