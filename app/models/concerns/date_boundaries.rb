module DateBoundaries
  extend ActiveSupport::Concern

  class_methods do
    def has_date_boundaries(from_date: :from_date,
                            to_date: :to_date,
                            approximate_dates: :approximate_dates)
      validate :"#{approximate_dates}_not_null",
        if: proc { |o| o.send(from_date).present? || o.send(to_date).present? }
      validate :"validate_date_boundaries_#{from_date}_#{to_date}"
      validate :"#{from_date}_not_in_the_future"
      validate :"#{to_date}_not_in_the_future"

      before_validation :"clear_#{approximate_dates}",
        if: proc { |o| o.send(from_date).nil? && o.send(to_date).nil? }

      define_method :"#{approximate_dates}_not_null" do
        return unless send(approximate_dates).nil?
        errors.add(approximate_dates, I18n.t("errors.messages.blank"))
      end

      define_method :"validate_date_boundaries_#{from_date}_#{to_date}" do
        return if send(from_date).nil?
        return if send(to_date).nil?
        return if send(from_date) <= send(to_date)
        errors.add(from_date, I18n.t("errors.out_of_bounds"))
        errors.add(to_date, I18n.t("errors.out_of_bounds"))
      end

      [from_date, to_date].each do |field|
        define_method :"#{field}_not_in_the_future" do
          return if send(field).nil?
          return if send(field).to_date <= Date.today
          errors.add(field, I18n.t("errors.cannot_be_in_the_future"))
        end
      end

      define_method :"clear_#{approximate_dates}" do
        send(:"#{approximate_dates}=", nil)
      end
    end
  end
end
