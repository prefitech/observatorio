module DefaultValue
  extend ActiveSupport::Concern

  class_methods do
    def default_value(attribute, value)
      after_initialize :"_set_default_#{attribute}"

      define_method :"_set_default_#{attribute}" do
        return unless has_attribute?(attribute)
        if self[attribute].nil?
          self[attribute] = (value.is_a?(Proc) ? value.call(self) : value)
        end
      end
    end
  end
end
