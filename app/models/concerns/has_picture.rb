module HasPicture
  extend ActiveSupport::Concern

  class_methods do
    def has_picture
      has_one :picture, {
        class_name: "Picture",
        as: :picturable,
        dependent: :destroy
      }
    end
  end
end
