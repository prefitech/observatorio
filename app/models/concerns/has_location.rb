module HasLocation
  extend ActiveSupport::Concern

  class_methods do
    def has_location(attribute = :location)
      belongs_to attribute,
                 -> { select(:id, :address) },
                 class_name: "Location",
                 foreign_key: :"#{attribute}_id",
                 validate: false
    end
  end
end
