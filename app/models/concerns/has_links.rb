module HasLinks
  extend ActiveSupport::Concern

  class_methods do
    def has_links(association = :links)
      has_many association, {
        class_name: "Link",
        as: :linkable,
        dependent: :destroy
      }

      validate :"#{association}_unique_label"

      define_method :"#{association}_unique_label" do
        labels = []
        send(association).each do |link|
          next unless link.label.present?
          if labels.include?(link.label.downcase)
            errors.add(:"#{association}.label", I18n.t("errors.messages.taken"))
            link.errors.add(:label, I18n.t("errors.messages.taken"))
          else
            labels << link.label.downcase
          end
        end
      end
    end
  end
end
