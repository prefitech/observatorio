class Education < ApplicationRecord
  has_paper_trail
  has_links             :sources
  has_attachments
  has_date_boundaries
  has_immutable_attributes :school_id, :student_id

  belongs_to :student, class_name: "Person"
  belongs_to :school,  class_name: "Organization"
  belongs_to :course,  class_name: "EducationCourse"
  belongs_to :degree,  class_name: "EducationDegree"

  validates_presence_of :student
  validates_presence_of :school
  validate              :assert_organization_is_school

  private

  def assert_organization_is_school
    return if school.nil? || school.school?
    errors.add(:school_id, errors.generate_message(:school, :should_be_school))
  end
end
