class TaggedPerson < ApplicationRecord
  self.table_name = "people_tags"

  has_paper_trail

  belongs_to :person
  belongs_to :tag, class_name: "PersonTag"
end
