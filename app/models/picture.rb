class Picture < ApplicationRecord
  has_paper_trail

  mount_uploader :file, CloudinaryUploader

  belongs_to :picturable, polymorphic: true

  has_immutable_attributes :file

  validates_presence_of :file
  validates_presence_of :picturable
  validates_uniqueness_of :picturable_id, scope: [:picturable_type]
  validate :valid_source_url

  before_validation :clear_source_text, {
    unless: proc { |p| p.source_url.nil? }
  }
  before_validation :download_file, {
    on: :create,
    unless: proc { |p| p.source_url.nil? }
  }

  def source
    source_url || source_text
  end

  def thumb
    file && file.thumb
  end

  def show
    file && file.show
  end

  def face
    file && file.face
  end

  def face_thumb
    file && file.face_thumb
  end

  private

  def clear_source_text
    self.source_text = nil
  end

  def download_file
    head = DOWNLOADER.head(source_url)
    return unless head.success?
    mime_type = Rack::Mime::MIME_TYPES[head.extension]
    return unless mime_type.present? && mime_type.starts_with?("image/")
    self.file = DOWNLOADER.get(source_url).file
  end

  def valid_source_url
    if source_url.present? && !file.present?
      errors.add(:source_url, I18n.t("errors.messages.invalid"))
    end
  end
end
