class Attachment < ApplicationRecord
  default_scope { order(:label) }

  has_paper_trail
  has_file

  belongs_to :attachable, polymorphic: true

  validates_presence_of   :label
  validates_presence_of   :attachable
  validates_uniqueness_of :label, {
    scope: [:attachable_id, :attachable_type],
    case_sensitive: false
  }

  after_create :create_attachment_inverse_relationship, {
    if: proc { |o| o.attachable.is_a?(Relationship) }
  }
  after_update :update_attachment_inverse_relationship, {
    if: proc { |o| o.saved_changes? && o.attachable.is_a?(Relationship) }
  }
  after_destroy :destroy_attachment_inverse_relationship, {
    if: proc { |o| o.attachable.is_a?(Relationship) }
  }

  private

  def create_attachment_inverse_relationship
    inverse_relationship = attachable.inverse_relationship
    return if Attachment.find_by(attachable: inverse_relationship, label: label).present?
    Attachment.create!(
      label: label,
      attachable: inverse_relationship,
      file: file,
      file_size: file_size,
      file_extension: file_extension,
      file_encryption_iv_base64: file_encryption_iv_base64
    )
  end

  def update_attachment_inverse_relationship
    old_label = saved_changes["label"].first
    inverse_attachment = Attachment.find_by(
      label: old_label, attachable: attachable.inverse_relationship
    )
    inverse_attachment.update_attributes!(label: label) if inverse_attachment.present?
  end

  def destroy_attachment_inverse_relationship
    inverse_attachment = Attachment.find_by(
      label: label, attachable: attachable.inverse_relationship
    )
    inverse_attachment.destroy! if inverse_attachment.present?
  end
end
