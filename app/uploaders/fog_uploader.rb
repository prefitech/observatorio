class FogUploader < CarrierWave::Uploader::Base
  storage :fog

  # Ensure unique filename https://frama.link/CarrierWaveUniqueFilenames
  def filename
    # ex: 4/e/4/7/4e47e683-f3a0-4b37-b2ea-cd6d25aa54bf
    "#{secure_token.chars[0..3].join("/")}/#{secure_token}" if original_filename.present?
  end

  def store_dir
    # root level
  end

  protected

  def secure_token
    var = :"@#{mounted_as}_secure_token"
    model.instance_variable_get(var) ||
      model.instance_variable_set(var, SecureRandom.uuid)
  end
end
