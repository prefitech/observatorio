collection @resources

attribute :id
attribute :name

node(:location) do |department|
  partial("locations/show", :object => department.location )
end
