class AttachmentsController < ResourcesController
  # GET /attachments
  def index
    super
    @resources = @resources.includes(:attachable)
    render :index, layout: false
  end

  # GET /attachments/:id
  def show
    super
    respond_to do |format|
      format.html { render :show }
      format.json { render :show }
    end
  end

  # GET /attachments/:id/download
  def download
    @resource = resources_class.find(params.require(:id))
    authorize! :read, @resource
    send_file(
      @resource.file_decrypted.path,
      filename: "#{@resource.label}-#{@resource.id}#{@resource.file_extension}"
    )
  end

  private

  def authorize!(action, resource)
    if action == :list && resource == Attachment
      attachable_id = params.require(:attachment).require(:attachable_id)
      attachable_type = params.require(:attachment).require(:attachable_type)
      authorize! :read, attachable_type.constantize.find(attachable_id)
    else
      super
    end
  end

  def resources_per_page
    nil
  end

  def resources_class
    Attachment
  end

  def versions_unsorted
    resource_versions
  end

  def index_params
    index_or_new_params
  end

  def new_params
    index_or_new_params
  end

  def index_or_new_params
    params.require(:attachment)
          .permit(:attachable_id,
                  :attachable_type)
  end

  def create_params
    uploaded_file = params.require(:attachment)[:file_unencrypted]
    file_extension = uploaded_file.present? ? File.extname(uploaded_file.path) : nil
    params.require(:attachment)
          .permit(:label,
                  :description,
                  :file_unencrypted,
                  :attachable_id,
                  :attachable_type)
          .merge(file_extension: file_extension)
  end

  def update_params
    params.require(:attachment)
          .permit(:label,
                  :description)
  end
end
