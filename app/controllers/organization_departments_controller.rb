class OrganizationDepartmentsController < ResourcesController
  layout false

  # GET /organization_departments
  def index
    super
    respond_to do |format|
      format.html do
        @resources = @resources
                       .includes(:location,
                                 members: [
                                   :member,
                                   :organization,
                                   :location,
                                   :sources,
                                   :attachments
                                 ])
      end
      format.json
    end
  end

  private

  def authorize!(action, resource)
    if action == :list && resource == OrganizationDepartment
      organization_id = params.require(:organization_department)
                              .require(:organization_id)
      organization = Organization.find(organization_id)
      authorize! :read, organization
    else
      super
    end
  end

  def resources_per_page
    nil
  end

  def resources_class
    OrganizationDepartment
  end

  def index_params
    index_or_new_params
  end

  def new_params
    index_or_new_params
  end

  def index_or_new_params
    params.require(:organization_department)
          .permit(:parent_id,
                  :organization_id)
          .tap do |hash|
      if hash.key?(:parent_id) && hash[:parent_id].empty?
        hash[:parent_id] = nil
      end
    end
  end

  def create_params
    params.require(:organization_department)
          .permit(:name,
                  :parent_id,
                  :location_id,
                  :organization_id)
  end

  def update_params
    params.require(:organization_department)
          .permit(:name,
                  :location_id)
  end
end
