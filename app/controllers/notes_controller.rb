class NotesController < ResourcesController
  layout false

  # GET /notes
  def index
    super
    @resources = @resources.includes(:author)
  end

  private

  def authorize!(action, resource)
    if action == :list && resource == Note
      subject_id = params.require(:note).require(:subject_id)
      subject_type = params.require(:note).require(:subject_type)
      authorize! :read, subject_type.constantize.find(subject_id)
    else
      super
    end
  end

  def resources_per_page
    nil
  end

  def resources_class
    Note
  end

  def index_params
    params.require(:note)
          .permit(:subject_id,
                  :subject_type)
  end

  def new_params
    index_params.merge(author_id: current_user.id)
  end

  def create_params
    params.require(:note)
          .permit(:body,
                  :subject_id,
                  :subject_type)
          .merge(author_id: current_user.id)
  end
end
