class OrganizationOwnersController < ResourcesController
  layout false

  # GET /organization_owners
  def index
    super
    @resources = @resources
                   .includes(:sources,
                             :attachments,
                             organization: [:names])
                   .order("names.value")
  end

  private

  def authorize!(action, resource)
    if action == :list && resource == OrganizationOwner
      owner_id = params.require(:organization_owner)[:owner_id]
      if owner_id.present?
        owner_klass = params.require(:organization_owner)
                            .require(:owner_type)
                            .constantize
        owner = owner_klass.find(owner_id)
        authorize! :read, owner
      else
        organization_id = params.require(:organization_owner)
                                .require(:organization_id)
        organization = Organization.find(organization_id)
        authorize! :read, organization
      end
    else
      super
    end
  end

  def resources_per_page
    nil
  end

  def resources_class
    OrganizationOwner
  end

  def index_params
    index_or_new_params
  end

  def new_params
    index_or_new_params
  end

  def index_or_new_params
    params.require(:organization_owner)
          .permit(:owner_id,
                  :owner_type,
                  :organization_id)
  end

  def create_params
    params.require(:organization_owner)
          .permit(:owner_id,
                  :owner_type,
                  :organization_id,
                  :portion_percentage,
                  :from_date,
                  :to_date,
                  :approximate_dates,)
  end

  def update_params
    params.require(:organization_owner)
          .permit(:portion_percentage,
                  :from_date,
                  :to_date,
                  :approximate_dates,)
  end
end
