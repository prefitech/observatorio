class PicturesController < ResourcesController
  layout false

  private

  def authorize!(action, resource)
    if action == :list && resource == Picture
      picturable_id = params.require(:picture).require(:picturable_id)
      picturable_type = params.require(:picture).require(:picturable_type)
      authorize! :read, picturable_type.constantize.find(picturable_id)
    else
      super
    end
  end

  def resources_class
    Picture
  end

  def resources_per_page
    nil
  end

  def index_params
    index_or_new_params
  end

  def new_params
    index_or_new_params
  end

  def index_or_new_params
    params.require(:picture)
          .permit(:picturable_id,
                  :picturable_type)
  end

  def create_params
    params.require(:picture)
          .permit(:file,
                  :author,
                  :license,
                  :source_url,
                  :source_text,
                  :picturable_id,
                  :picturable_type)
  end

  def update_params
    params.require(:picture)
          .permit(:author,
                  :license,
                  :source_text)
  end
end
