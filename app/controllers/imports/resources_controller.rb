class Imports::ResourcesController < ResourcesController
  # POST /imports/[collection]
  def create
    @resource = resources_class.new(create_params)
    authorize! :create, @resource
    if @resource.save
      flash[:success] = Import.human_attribute_name(:created)
      redirect_to url_for(controller: controller_name,
                          action: "show",
                          id: @resource.id,
                          only_path: true)
      import_job_class.perform_later(@resource.id)
    else
      flash[:error] = @resource.errors.first[1]
      redirect_to url_for(controller: controller_name, action: "index")
    end
  end

  # GET /imports/[collection]/:id/download
  def download
    @resource = resources_class.find(params.require(:id))
    authorize! :read, @resource
    send_file(@resource.file_decrypted.path,
              filename: "#{@resource.display_name.gsub(/\W/, "_")}#{@resource.file_extension}")
  end

  private

  def import_job_class
    raise NotImplementedError
  end

  def create_params
    uploaded_file = params.require(:import)[:file_unencrypted]
    file_extension = uploaded_file.present? ? File.extname(uploaded_file.path) : nil
    params.require(:import)
          .permit(:file_unencrypted)
          .merge(user_id: current_user.id,
                 file_extension: file_extension)
  end
end
