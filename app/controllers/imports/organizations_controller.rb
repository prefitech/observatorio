class Imports::OrganizationsController < Imports::ResourcesController
  private

  def resources_class
    OrganizationsImport
  end

  def import_job_class
    ImportOrganizationsJob
  end
end
