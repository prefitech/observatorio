class Imports::MembershipsController < Imports::ResourcesController
  private

  def resources_class
    MembershipsImport
  end

  def import_job_class
    ImportMembershipsJob
  end
end
