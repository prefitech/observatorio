class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :authenticate_user!
  before_action :set_timezone
  before_action :set_paper_trail_whodunnit, if: :user_signed_in?

  check_authorization unless: :skip_authorization?

  rescue_from CanCan::AccessDenied do |e|
    respond_to do |format|
      format.html { render :forbidden, status: 403 }
      format.json { render :forbidden, status: 403 }
    end
  end

  rescue_from ActionController::ParameterMissing do |e|
    @error_message = e.message
    respond_to do |format|
      format.html { render :bad_request, status: 400 }
      format.json { render :bad_request, status: 400 }
    end
  end

  private

  def set_timezone
    Time.zone = user_signed_in? ?
      current_user.time_zone : APP_CONFIG["default_time_zone"]
  end

  def skip_authorization?
    devise_controller?
  end
end
