class LocationsController < ResourcesController
  respond_to :json

  rescue_from Geocoder::GeocodingError do |e|
    @error_message = e.message
    render :bad_request, status: 400
  end

  # GET /locations/:id
  def show
    @resource = resources_class.find(params.require(:id))
    authorize! :read, @resource
  end

  # POST /locations/reverse_geocode
  def reverse_geocode
    authorize! :create, Location
    @resource = begin
      if location_params[:lat] && location_params[:lng]
        Location.find_by(lat: location_params[:lat],
                         lng: location_params[:lng])
      else
        Location.find_by(osm_id: location_params[:osm_id],
                         osm_type: location_params[:osm_type],
                         lat: nil, lng: nil)
      end
    end
    @resource ||= begin
      geocoding_response = GEOCODER.reverse(geocoding_params.merge(language: I18n.locale))
      Location.create!(osm_id: geocoding_response[:osm_id],
                       osm_type: geocoding_response[:osm_type],
                       lat: geocoding_response[:lat],
                       lng: geocoding_response[:lng],
                       boundingbox: geocoding_response[:boundingbox],
                       address: geocoding_response[:address],
                       geojson: geocoding_response[:geojson])
    end
    render :show
  end

  private

  def resources_class
    Location
  end

  def location_params
    params.require(:location)
  end

  def geocoding_params
    if location_params.key?(:lat) || location_params.key?(:lng)
      return {
        lat: location_params.require(:lat),
        lng: location_params.require(:lng)
      }
    end
    return {
      osm_id: location_params.require(:osm_id),
      osm_type: location_params.require(:osm_type)
    }
  end
end
