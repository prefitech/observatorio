function initEventForm(modal) {
  // Configure datetimes
  var startedAtInput = $('input[name="event[started_at]"]', modal);
  var finishedAtInput = $('input[name="event[finished_at]"]', modal);
  function refreshStartedAt() {
    var timeZone = $('select[name="event[time_zone]"]', modal).val() || 'Etc/UTC';
    var startedAtDate = $('input[name="event[started_at_date]"]', modal).val();
    if (startedAtDate) {
      var startedAtTime = $('input[name="event[started_at_time]"]', modal).val();
      startedAtInput.val(moment.tz(startedAtDate + (startedAtTime ? ' ' + startedAtTime : ''), timeZone).format());
    } else {
      startedAtInput.val('');
    }
  }
  function refreshFinishedAt() {
    var timeZone = $('select[name="event[time_zone]"]', modal).val() || 'Etc/UTC';
    var finishedAtDate = $('input[name="event[finished_at_date]"]', modal).val();
    if (finishedAtDate) {
      var finishedAtTime = $('input[name="event[finished_at_time]"]', modal).val();
      finishedAtInput.val(moment.tz(finishedAtDate + (finishedAtTime ? ' ' + finishedAtTime : ''), timeZone).format());
    } else {
      finishedAtInput.val('');
    }
  }

  $('input[name="event[started_at_date]"],' +
    'input[name="event[started_at_time]"],' +
    'input[name="event[finished_at_date]"],' +
    'input[name="event[finished_at_time]"],' +
    'select[name="event[time_zone]"]', modal).change(function(event) {
    refreshStartedAt();
    refreshFinishedAt();
  });
}
