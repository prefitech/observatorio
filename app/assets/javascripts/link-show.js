$(document).ready(function() {
  var showLinks = $('.links.show');
  var linkContent = $('.link-content', showLinks);
  var loadingContent = $('.loading-link', showLinks);
  var label = $('.link-label', linkContent);
  var description = $('.link-description', linkContent);
  $('.modal-form', showLinks).on('hidden.bs.modal', function(event) {
    var modal = $(event.target);
    var refreshLink = $('input[name="refresh_element"][value="#history-subsection"]', modal);
    if (refreshLink.length) {
      linkContent.css('visibility', 'hidden');
      loadingContent.show();
      $.ajax({
        url: linkContent.attr('data-url'),
        dataType: 'json',
        success: function(json) {
          setTimeout(function() {
            label.html(json.label);
            if (json.description) {
              description.html(json.description.replaceAll('\n', '</br>'));
            } else {
              description.empty();
            }
            loadingContent.hide();
            linkContent.css('visibility', 'visible');
          }, 400);
        }
      });
    }
  });
});
