$(document).ready(function() {
  if (!$('.show.imports').length) return;
  // Animate logs loading
  function animateLoading() {
    var showImports = $('.show.imports');
    var element = $('.loading', showImports);
    if (!element.length) return;
    if (!$('.svg-white', element).length) {
      $('.svg-secondary', element).first().removeClass('svg-secondary').addClass('svg-white');
    } else {
      var highlighted = $('.svg-white', element);
      highlighted.removeClass('svg-white').addClass('svg-secondary');
      highlighted.next().removeClass('svg-secondary').addClass('svg-white');
    }
    setTimeout(animateLoading, 200);
  }
  setTimeout(animateLoading, 200);

  // Autorefresh
  function autoRefresh() {
    var showImports = $('.show.imports');
    if ($('.status-badge', showImports).hasClass('status-finished')) return;
    $.ajax({
      url: window.location.pathname,
      success: function(data) {
        var html = $(data);
        $('.status-badge', showImports).replaceWith($('.status-badge', html));
        $('.import-logs', showImports).replaceWith($('.import-logs', html));
        setTimeout(autoRefresh, 2000);
      }
    });
  }
  setTimeout(autoRefresh, 2000);
});
