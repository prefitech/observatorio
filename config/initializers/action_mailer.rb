require_relative "log_strategy"

ActionMailer::Base.default_url_options = { host: APP_CONFIG["mailer"]["host"] }
ActionMailer::Base.delivery_method = APP_CONFIG["mailer"]["delivery_method"].to_sym
ActionMailer::Base.raise_delivery_errors = APP_CONFIG["mailer"]["raise_delivery_errors"]
ActionMailer::Base.logger = LogStrategy.logger
if APP_CONFIG["mailer"]["default_url_options"]
  ActionMailer::Base.default_url_options = APP_CONFIG["mailer"]["default_url_options"].symbolize_keys
end
if APP_CONFIG["mailer"]["smtp_settings"]
  ActionMailer::Base.smtp_settings = APP_CONFIG["mailer"]["smtp_settings"].symbolize_keys
end
