require "cloudinary"
require_relative "log_strategy"

Cloudinary.config do |config|
  config.secure = true
  if Rails.env.test?
    # Mock Cloudinary
    class Cloudinary::Uploader
      def self.destroy(public_id, options={})
        LogStrategy.logger.info "File #{public_id} would be removed in Cloudinary #{options}"
      end
    end
    class Cloudinary::CarrierWave::Storage
      def store!(*args)
        LogStrategy.logger.info "File would be uploaded in Cloudinary #{args}"
      end
    end
    config.cloud_name = "observatorio-mock"
  end
end
