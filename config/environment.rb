# Load the Rails application.
require_relative 'application'

# Initialize the Rails application.
Rails.application.initialize!

# Configure logger
Rails.logger = LogStrategy.logger
ActiveRecord::Base.logger = LogStrategy.logger
