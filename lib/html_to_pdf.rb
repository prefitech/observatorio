class HtmlToPdf
  Error = Class.new(StandardError)

  class Content
    attr_reader :file, :extension, :visited_at, :http_status, :url

    def initialize(pdf_data = nil, http_status = nil)
      return unless pdf_data.present?
      @visited_at = DateTime.now
      @file = Tempfile.open(encoding: "ASCII-8BIT")
      @file.write(pdf_data)
      @file.rewind
      @extension = ".pdf"
    end
  end

  def initialize(logger)
    @logger = logger
  end

  def convert(url, options = {})
    Content.new(PDFKit.new(url, options).to_pdf, 200)
  rescue RuntimeError => e
    @logger.warn("Unable to convert #{url}: #{e.message}")
    unless options[:disable_javascript]
      @logger.warn("Trying with no JavaScript...")
      return convert(url, options.merge(disable_javascript: true))
    end
    raise HtmlToPdf::Error
  end
end
