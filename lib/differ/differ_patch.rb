require "differ"

module Differ
  class << self
    def diff(target, source, separator = "\n")
      target = "" if target.nil?
      source = "" if source.nil?

      old_sep, $; = $;, separator

      @diff = Diff.new(target, source)

      target = target.split(separator)
      source = source.split(separator)

      $; = '' if separator.is_a? Regexp

      advance(target, source) until source.empty? || target.empty?
      @diff.insert(*target) || @diff.delete(*source)
      return @diff
    ensure
      $; = old_sep
    end
  end
end
