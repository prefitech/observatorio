require "differ/diff"

# Monkey patch differ gem in order to display
module Differ
  class Diff
    def initialize(target, source)
      @target = target
      @source = source
      @raw = []
    end

    def format_as(format)
      if format == :html
        format_html
      else
        f = Differ.format_for(format)
        @raw.inject('') do |sum, part|
          part = case part
            when String then part
            when Change then f.format(part)
          end
          sum << part
        end
      end
    end

    private

    def format_html
      deletes = []
      inserts = []
      formatted = @raw.each_with_object("") do |step, text|
        next if step.respond_to?(:blank?) && step.blank?
        case step
        when String
          if deletes.present?
            text << "<span class='changeset delete'>#{deletes.join(" ")}</span>"
            deletes = []
          end
          if inserts.present?
            text << "<span class='changeset insert'>#{inserts.join(" ")}</span>"
            inserts = []
          end
          text << step
        when Change
          if step.delete? || step.change?
            deletes << step.delete
          end
          if step.insert? || step.change?
            inserts << step.insert
          end
        end
      end
      if deletes.present?
        formatted << "<span class='changeset delete'>#{deletes.join(" ")}</span>"
        deletes = []
      end
      if inserts.present?
        formatted << "<span class='changeset insert'>#{inserts.join(" ")}</span>"
        inserts = []
      end
      formatted
    end
  end
end
