# HEROKU DEPLOYMENT GUIDE

#### Pre-requisites

- Install [Git](https://git-scm.com/downloads)
- Have a [Mapbox account](https://www.mapbox.com/signin/) (Mapbox's free of charge plan [allows 50,000 map views per month](https://www.mapbox.com/pricing/))
- Have a [Cloudinary account](https://cloudinary.com/users/login) (Cloudinary's free of charge plan [allows 300,000 images & videos](https://cloudinary.com/pricing))
- Have access to any [OpenStack Swift provider](https://www.openstack.org/marketplace/public-clouds/f/a/Object%20store%20-%20Swift) __*__
- Have a dedicated e-mail account with [SMTP access](https://en.wikipedia.org/wiki/Simple_Mail_Transfer_Protocol) __*__
- Have a [Heroku account](https://signup.heroku.com/login)
- Install the [Heroku command line interface](https://devcenter.heroku.com/articles/heroku-cli#download-and-install)
- [Log in](https://devcenter.heroku.com/articles/heroku-cli#getting-started) to your Heroku account using CLI

__*__ Optional for testing

#### Create an application with Ruby buildpack

```sh
heroku create <choose-a-name> --buildpack https://github.com/heroku/heroku-buildpack-ruby.git
```

#### Add wkhtmltopdf buildpack

```sh
heroku buildpacks:add -i 1 https://framagit.org/lobster/heroku-buildpack-wkhtmltopdf -a <your-application-name>
```

#### Provision a PostgreSQL server

Install [Heroku Postgres](https://elements.heroku.com/addons/heroku-postgresql) add-on on your application

```sh
heroku addons:create heroku-postgresql:<choose-a-plan> -a <your-application-name>
```

#### Set environment variables

See [environment variables table](https://framagit.org/lobster/observatorio/blob/develop/docs/ENVIRONMENT_VARIABLES.md).

#### Download & deploy source code

```sh
git clone https://framagit.org/lobster/observatorio --branch master && cd observatorio/
heroku git:remote -a <your-application-name>
git push heroku master
```

#### Initialize database

```sh
heroku run rake db:migrate observatorio:setup -a <your-application-name>
```

#### Enjoy!

Visit `https://<your-application-name>.herokuapps.com/`
