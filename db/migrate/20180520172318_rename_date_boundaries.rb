class RenameDateBoundaries < ActiveRecord::Migration[5.0]
  def change
    [:educations, :memberships, :relationships].each do |table|
      rename_column table, :since_date, :from_date
      rename_column table, :until_date, :to_date
    end
  end
end
