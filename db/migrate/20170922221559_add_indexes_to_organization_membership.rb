class AddIndexesToOrganizationMembership < ActiveRecord::Migration[5.0]
  def change
    add_index :organization_memberships, [:person_id]
    add_index :organization_memberships, [:organization_id]
  end
end
