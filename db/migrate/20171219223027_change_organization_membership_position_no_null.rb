class ChangeOrganizationMembershipPositionNoNull < ActiveRecord::Migration[5.0]
  def change
    change_column_null :organization_memberships, :position_id, true
  end
end
