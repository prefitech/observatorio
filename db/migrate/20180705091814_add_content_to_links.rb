class AddContentToLinks < ActiveRecord::Migration[5.0]
  def change
    add_column :links, :content,               :text
    add_column :links, :content_extension,     :text
    add_column :links, :content_size,          :integer, limit: 8
    add_column :links, :content_encryption_iv, :text
    add_column :links, :visited_at,            :datetime
  end
end
