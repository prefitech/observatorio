class ChangeIndexOrganizationMembership < ActiveRecord::Migration[5.0]
  def change
    remove_index :organization_ownerships, :owner_id
    add_index    :organization_ownerships, [:owner_id, :owner_type]
  end
end
