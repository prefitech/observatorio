class AddUniqueIndexToPersonTags < ActiveRecord::Migration[5.0]
  def change
    add_index :person_tags, "lower(name) varchar_pattern_ops", unique: true
  end
end
