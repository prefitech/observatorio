class CreateEducationDegrees < ActiveRecord::Migration[5.0]
  def change
    create_table :education_degrees, id: :uuid do |t|
      t.timestamps null: false
      t.text       :name, null: false
      t.integer    :display_priority, null: false
    end
    add_index :education_degrees, :name, unique: true
  end
end
