class AddIgnoreContentToLinks < ActiveRecord::Migration[5.2]
  def change
    add_column :links, :ignore_content, :boolean
  end
end
