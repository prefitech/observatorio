class RenameEventParticipantsIntoEventParticipations < ActiveRecord::Migration[5.0]
  def change
    rename_table :event_participants, :event_participations
  end
end
