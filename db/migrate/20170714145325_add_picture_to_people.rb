class AddPictureToPeople < ActiveRecord::Migration[5.0]
  def change
    add_column :people, :picture, :text
  end
end
