class AddCollationToPersonTagsName < ActiveRecord::Migration[5.0]
  def up
    change_column :person_tags, :name, :text, collation: I18n.locale.to_s.gsub("-", "_")
  end

  def down
    change_column :person_tags, :name, :text
  end
end
