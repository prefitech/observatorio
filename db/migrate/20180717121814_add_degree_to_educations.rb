class AddDegreeToEducations < ActiveRecord::Migration[5.0]
  def change
    add_column :educations, :degree_id, :uuid
  end
end
