class AddLocationToOrganizationMembership < ActiveRecord::Migration[5.0]
  def change
    add_column :organization_memberships, :location, :json
  end
end
