class AddCourseIdToEducations < ActiveRecord::Migration[5.0]
  def change
    add_column :educations, :course_id, :uuid, null: true
  end
end
