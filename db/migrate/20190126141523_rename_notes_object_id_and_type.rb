class RenameNotesObjectIdAndType < ActiveRecord::Migration[5.2]
  def change
    rename_column :notes, :object_id, :subject_id
    rename_column :notes, :object_type, :subject_type
  end
end
