class FixOrganizationOwnershipIndexes < ActiveRecord::Migration[5.0]
  def change
    remove_index :organization_ownerships, [:person_id, :organization_id]
    add_index    :organization_ownerships, [:person_id]
    add_index    :organization_ownerships, [:organization_id]
  end
end
