class AddLocationForeignKeyToOrganizations < ActiveRecord::Migration[5.0]
  def change
    add_column :organizations, :headquarters_id, :uuid
    rename_column :organizations, :headquarters, :headquarters_old
  end
end
