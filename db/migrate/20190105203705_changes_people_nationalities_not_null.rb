class ChangesPeopleNationalitiesNotNull < ActiveRecord::Migration[5.1]
  def change
    reversible do |dir|
      dir.up { execute "UPDATE people SET nationalities = '{}' WHERE nationalities IS NULL" }
    end
    change_column_null :people, :nationalities, false
  end
end
