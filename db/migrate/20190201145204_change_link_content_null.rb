class ChangeLinkContentNull < ActiveRecord::Migration[5.2]
  def change
    change_column_null :links, :content, true
    change_column_null :links, :content_size, true
    change_column_null :links, :content_encryption_iv_base64, true
  end
end
