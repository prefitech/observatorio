class CreateEventRoles < ActiveRecord::Migration[5.0]
  def change
    create_table :event_roles, id: :uuid do |t|
      t.timestamps                    null: false
      t.uuid       :event_id,         null: false
      t.text       :name,             null: false
      t.integer    :display_priority, null: false
    end
    add_index :event_roles, [:event_id, :name], unique: true
    add_column :event_participations, :role_id, :uuid
    reversible do |dir|
      dir.up do
        execute <<-SQL
          INSERT INTO event_roles (created_at, updated_at, event_id, name, display_priority)
            SELECT now(), now(), event_participations.event_id, participation_categories.name, 0
              FROM event_participations
              INNER JOIN participation_categories ON event_participations.category_id = participation_categories.id
              GROUP BY event_participations.event_id, participation_categories.name;
          UPDATE event_participations SET role_id = event_roles.id
            FROM event_roles
            INNER JOIN participation_categories ON event_roles.name = participation_categories.name
            WHERE participation_categories.id = event_participations.category_id AND
                  event_roles.event_id = event_participations.event_id;
        SQL
      end
      dir.down do
        execute <<-SQL
          UPDATE event_participations SET category_id = participation_categories.id
            FROM event_roles
            INNER JOIN participation_categories ON event_roles.name = participation_categories.name
            WHERE event_roles.id = event_participations.role_id;
        SQL
      end
    end
    change_column_null :event_participations, :role_id, false
    remove_column :event_participations, :category_id, :uuid
    drop_table :participation_categories, id: :uuid do |t|
      t.timestamps                    null: false
      t.text       :name,             null: false, unique: true
      t.integer    :display_priority, null: false
    end
  end
end
