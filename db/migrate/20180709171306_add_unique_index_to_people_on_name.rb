class AddUniqueIndexToPeopleOnName < ActiveRecord::Migration[5.0]
  def change
    remove_index :people, [:first_name, :last_name]
    add_index :people, "lower(first_name) varchar_pattern_ops, lower(last_name) varchar_pattern_ops", unique: true, name: "index_people_on_full_name_case_insensitive"
  end
end
