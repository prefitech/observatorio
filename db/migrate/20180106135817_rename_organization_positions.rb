class RenameOrganizationPositions < ActiveRecord::Migration[5.0]
  def change
    rename_table :organization_positions, :positions
    reversible do |dir|
      dir.up do
        execute "UPDATE versions SET item_type = 'Position' WHERE item_type = 'OrganizationPosition'"
      end
      dir.down do
        execute "UPDATE versions SET item_type = 'OrganizationPosition' WHERE item_type = 'Position'"
      end
    end
  end
end
