class UpgradeVersions < ActiveRecord::Migration[5.1]
  def change
    add_column :versions, :item_subtype, :string
  end
end
