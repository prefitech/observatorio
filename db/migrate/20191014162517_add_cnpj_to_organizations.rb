class AddCnpjToOrganizations < ActiveRecord::Migration[5.2]
  def change
    add_column :organizations, :cnpj, :text
    add_index  :organizations, :cnpj
  end
end
