class FixNotesIndex < ActiveRecord::Migration[5.0]
  def change
    remove_index :notes, [:object_id, :object_type]
    add_index :notes, :object_id
  end
end
