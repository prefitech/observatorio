class RecreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events, id: :uuid do |t|
      t.timestamps                     null: false
      t.text       :name,              null: false, collation: I18n.locale.to_s.gsub("-", "_")
      t.datetime   :started_at
      t.datetime   :finished_at
      t.boolean    :approximate_dates, null: false
      t.text       :time_zone,         null: false
      t.uuid       :location_id
    end
    add_index :events, "lower(name) varchar_pattern_ops", unique: true
    create_table :event_roles, id: :uuid do |t|
      t.timestamps                    null: false
      t.text       :name,             null: false
      t.integer    :display_priority, null: false
    end
    add_index :event_roles, "lower(name) varchar_pattern_ops", unique: true
    create_table :events_participants, id: :uuid do |t|
      t.timestamps                     null: false
      t.uuid       :event_id,          null: false
      t.uuid       :role_id
      t.uuid       :participant_id,    null: false
      t.text       :participant_type,  null: false
      t.datetime   :from_datetime
      t.datetime   :to_datetime
      t.boolean    :approximate_dates, null: false
      t.text       :time_zone
      t.uuid       :location_id
    end
    add_index :events_participants, [:event_id, :participant_id, :location_id], name: "index_to_events_participants_unique_partial"
    add_index :events_participants, :participant_id
    create_table :event_tags, id: :uuid do |t|
      t.timestamps                    null: false
      t.text       :name,             null: false, collation: I18n.locale.to_s.gsub("-", "_")
      t.integer    :display_priority, null: false
    end
    add_index :event_tags, "lower(name) varchar_pattern_ops", unique: true
    create_table :events_event_tags, id: false do |t|
      t.timestamps                null: false
      t.uuid       :event_id,     null: false
      t.uuid       :event_tag_id, null: false
    end
    add_index :events_event_tags, :event_id
    add_index :events_event_tags, :event_tag_id
  end
end
