class AddDepartmentToMemberships < ActiveRecord::Migration[5.0]
  def change
    add_column :memberships, :department_id, :uuid
  end
end
