class CreatePictures < ActiveRecord::Migration[5.2]
  def change
    create_table :pictures, id: :uuid do |t|
      t.timestamps                  null: false
      t.text       :file,           null: false
      t.text       :source_url
      t.text       :source_text
      t.text       :author
      t.text       :license
      t.uuid       :picturable_id,   null: false
      t.text       :picturable_type, null: false
    end
    add_index :pictures, [:picturable_id, :picturable_type], unique: true
    reversible do |dir|
      dir.up do
        execute <<-SQL
          INSERT INTO pictures (created_at, updated_at, file,
                              picturable_id, picturable_type)
            SELECT now(), now(), picture, id, 'Person'
              FROM people WHERE people.picture IS NOT NULL;
          INSERT INTO pictures (created_at, updated_at, file,
                              picturable_id, picturable_type)
            SELECT now(), now(), picture, id, 'Organization'
              FROM organizations WHERE organizations.picture IS NOT NULL;
        SQL
      end
      dir.down do
        execute <<-SQL
          UPDATE people SET picture = pictures.file
            FROM pictures
              WHERE pictures.picturable_id = people.id AND
                    pictures.picturable_type = 'Person';
          UPDATE organizations SET picture = pictures.file
            FROM pictures
              WHERE pictures.picturable_id = organizations.id AND
                    pictures.picturable_type = 'Organization';
        SQL
      end
    end
    remove_column :people, :picture, :text
    remove_column :organizations, :picture, :text
  end
end
