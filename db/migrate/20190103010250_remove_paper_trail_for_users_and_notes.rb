class RemovePaperTrailForUsersAndNotes < ActiveRecord::Migration[5.1]
  def up
    execute "DELETE FROM versions WHERE item_type = 'User'"
    execute "DELETE FROM versions WHERE item_type = 'Note'"
  end
end
