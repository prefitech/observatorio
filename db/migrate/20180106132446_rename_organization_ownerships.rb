class RenameOrganizationOwnerships < ActiveRecord::Migration[5.0]
  def change
    rename_table :organization_ownerships, :ownerships
    reversible do |dir|
      dir.up do
        execute "UPDATE versions SET item_type = 'Ownership' WHERE item_type = 'OrganizationOwnership'"
      end
      dir.down do
        execute "UPDATE versions SET item_type = 'OrganizationOwnership' WHERE item_type = 'Ownership'"
      end
    end
  end
end
