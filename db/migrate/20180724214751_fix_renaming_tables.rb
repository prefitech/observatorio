class FixRenamingTables < ActiveRecord::Migration[5.0]
  def up
    execute <<-SQL
      UPDATE versions SET item_type = 'OrganizationPosition' WHERE item_type = 'Position';
      UPDATE versions SET item_type = 'OrganizationMember' WHERE item_type = 'Membership';
      UPDATE versions SET item_type = 'OrganizationOwner' WHERE item_type = 'Ownership';
      UPDATE links SET object_type = 'OrganizationMember' WHERE object_type = 'Membership';
      UPDATE links SET object_type = 'OrganizationOwner' WHERE object_type = 'Ownership';
    SQL
  end

  def down
    execute <<-SQL
      UPDATE versions SET item_type = 'Position' WHERE item_type = 'OrganizationPosition';
      UPDATE versions SET item_type = 'Membership' WHERE item_type = 'OrganizationMember';
      UPDATE versions SET item_type = 'Ownership' WHERE item_type = 'OrganizationOwner';
      UPDATE links SET object_type = 'Membership' WHERE object_type = 'OrganizationMember';
      UPDATE links SET object_type = 'Ownership' WHERE object_type = 'OrganizationMember';
    SQL
  end
end
