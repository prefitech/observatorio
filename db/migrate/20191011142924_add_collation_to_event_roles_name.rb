class AddCollationToEventRolesName < ActiveRecord::Migration[5.2]
  def up
    change_column :event_roles, :name, :text, collation: I18n.locale.to_s.gsub("-", "_")
  end

  def down
    change_column :event_roles, :name, :text, collation: nil
  end
end
