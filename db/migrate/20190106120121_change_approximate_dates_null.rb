class ChangeApproximateDatesNull < ActiveRecord::Migration[5.1]
  def change
    reversible do |dir|
      change_column_null :people, :approximate_dates, true
      change_column_null :organizations, :approximate_dates, true
      change_column_null :relationships, :approximate_dates, true
      change_column_null :educations, :approximate_dates, true
      change_column_null :organizations_members, :approximate_dates, true
      change_column_null :events, :approximate_dates, true
      change_column_null :events_participants, :approximate_dates, true
      dir.up do
        execute <<-SQL
          UPDATE people SET approximate_dates = NULL
            WHERE date_of_birth IS NULL AND date_of_death IS NULL;
          UPDATE organizations SET approximate_dates = NULL
            WHERE date_of_foundation IS NULL AND date_of_extinction IS NULL;
          UPDATE relationships SET approximate_dates = NULL
            WHERE from_date IS NULL AND to_date IS NULL;
          UPDATE educations SET approximate_dates = NULL
            WHERE from_date IS NULL AND to_date IS NULL;
          UPDATE organizations_members SET approximate_dates = NULL
            WHERE from_date IS NULL AND to_date IS NULL;
          UPDATE events SET approximate_dates = NULL
            WHERE started_at IS NULL AND finished_at IS NULL;
        SQL
      end
      dir.down do
        execute <<-SQL
          UPDATE people SET approximate_dates = FALSE
            WHERE approximate_dates IS NULL;
          UPDATE organizations SET approximate_dates = FALSE
            WHERE approximate_dates IS NULL;
          UPDATE relationships SET approximate_dates = FALSE
            WHERE approximate_dates IS NULL;
          UPDATE educations SET approximate_dates = FALSE
            WHERE approximate_dates IS NULL;
          UPDATE organizations_members SET approximate_dates = FALSE
            WHERE approximate_dates IS NULL;
          UPDATE events SET approximate_dates = FALSE
            WHERE approximate_dates IS NULL;
        SQL
      end
    end
  end
end
