class CreateOrganizations < ActiveRecord::Migration[5.0]
  def change
    create_table :organizations, id: :uuid do |t|
      t.timestamps        null: false
      t.text       :name, null: false
      t.text       :kind, null: false
    end
    add_index :organizations, [:name, :kind], unique: true
  end
end
