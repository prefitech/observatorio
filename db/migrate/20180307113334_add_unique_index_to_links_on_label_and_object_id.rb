class AddUniqueIndexToLinksOnLabelAndObjectId < ActiveRecord::Migration[5.0]
  def change
    remove_index :links, [:object_id, :object_type]
    add_index    :links, [:object_id, :object_type, :label], unique: true
  end
end
