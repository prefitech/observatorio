class RenameOrganizationMemberships < ActiveRecord::Migration[5.0]
  def change
    rename_table  :organization_memberships, :memberships
    rename_column :memberships, :person_id, :member_id
    reversible do |dir|
      dir.up do
        execute "UPDATE versions SET item_type = 'Membership' WHERE item_type = 'OrganizationMembership'"
      end
      dir.down do
        execute "UPDATE versions SET item_type = 'OrganizationMembership' WHERE item_type = 'Membership'"
      end
    end
  end
end
