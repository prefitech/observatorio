class CreateNotes < ActiveRecord::Migration[5.0]
  def change
    create_table :notes, id: :uuid do |t|
      t.timestamps               null: false
      t.uuid       :author_id,   null: false
      t.uuid       :object_id,   null: false
      t.text       :object_type, null: false
      t.text       :body,        null: false
    end
    add_index :notes, [:object_id, :object_type]
  end
end
