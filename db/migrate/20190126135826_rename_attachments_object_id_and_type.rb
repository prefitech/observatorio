class RenameAttachmentsObjectIdAndType < ActiveRecord::Migration[5.2]
  def change
    rename_column :attachments, :object_id, :attachable_id
    rename_column :attachments, :object_type, :attachable_type
  end
end
