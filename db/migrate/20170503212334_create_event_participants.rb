class CreateEventParticipants < ActiveRecord::Migration[5.0]
  def change
    create_table :event_participants, id: :uuid do |t|
      t.uuid :event_id,         null: false
      t.uuid :participant_id,   null: false
      t.text :participant_type, null: false
    end
    add_index :event_participants, [:event_id, :participant_id, :participant_type], name: :event_participants_unique, unique: true
  end
end
