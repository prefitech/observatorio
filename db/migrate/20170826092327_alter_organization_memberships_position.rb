class AlterOrganizationMembershipsPosition < ActiveRecord::Migration[5.0]
  def change
    add_column :organization_memberships, :position_id, :uuid
    reversible do |dir|
      dir.up do
        execute <<-SQL
          INSERT INTO organization_positions (created_at, updated_at, organization_id, name, display_priority)
            SELECT now(), now(), organization_id, position, 0
              FROM organization_memberships
              WHERE length(position) > 0
              group by position, organization_id;
          UPDATE organization_memberships SET position_id = organization_positions.id
            FROM organization_positions
            WHERE organization_positions.name = organization_memberships.position AND
                  organization_positions.organization_id = organization_memberships.organization_id;
          UPDATE organization_memberships SET position_id = organization_positions.id
            FROM organization_positions
            WHERE organization_memberships.position_id IS NULL AND
                  organization_positions.name = '#{I18n.t("observatorio.other")}' AND
                  organization_positions.organization_id = organization_memberships.organization_id;

        SQL
      end
      dir.down do
        execute <<-SQL
          UPDATE organization_memberships SET position = organization_positions.name
            FROM organization_positions
            WHERE organization_positions.id = organization_memberships.position_id AND
                  organization_positions.name != '#{I18n.t("observatorio.other")}';
        SQL
      end
    end
    change_column_null :organization_memberships, :position_id, false
    remove_column      :organization_memberships, :position, :text
  end
end
