class FixEducationDegreesUniqueIndex < ActiveRecord::Migration[5.0]
  def change
    remove_index :education_degrees, [:name]
    add_index :education_degrees, "lower(name) varchar_pattern_ops", unique: true
  end
end
