class AddIndexToNamesOnNameableTypeAndMain < ActiveRecord::Migration[5.2]
  def change
    add_index :names, [:nameable_type, :main]
  end
end
