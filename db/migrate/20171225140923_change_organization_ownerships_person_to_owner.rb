class ChangeOrganizationOwnershipsPersonToOwner < ActiveRecord::Migration[5.0]
  def change
    rename_column :organization_ownerships, :person_id, :owner_id
    add_column    :organization_ownerships, :owner_type, :text
    reversible do |dir|
      dir.up do
        execute "UPDATE organization_ownerships SET owner_type = 'Person'"
      end
    end
    change_column_null :organization_ownerships, :owner_type, false
  end
end
