class FixAttachmentsUniqueIndex < ActiveRecord::Migration[5.0]
  def change
    remove_index :attachments, [:object_id, :object_type, :label]
    add_index :attachments, "object_id, object_type, lower(label) varchar_pattern_ops", unique: true, name: "index_people_on_object_and_label_case_insensitive"
  end
end
