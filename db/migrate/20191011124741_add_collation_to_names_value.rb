class AddCollationToNamesValue < ActiveRecord::Migration[5.2]
  def up
    change_column :names, :value, :text, collation: I18n.locale.to_s.gsub("-", "_")
  end

  def down
    change_column :names, :value, :text, collation: nil
  end
end
