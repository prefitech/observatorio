class ChangeLinksContentForbiddenNotNull < ActiveRecord::Migration[5.2]
  def change
    reversible do |dir|
      dir.up do
        execute <<-SQL
          UPDATE links SET content_forbidden = FALSE
            WHERE content_forbidden IS NULL
        SQL
      end
    end
    change_column_null :links, :content_forbidden, false
  end
end
