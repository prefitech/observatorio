class FixOwnershipsIndexes < ActiveRecord::Migration[5.0]
  def change
    remove_index :ownerships, [:owner_id, :owner_type]
  end
end
