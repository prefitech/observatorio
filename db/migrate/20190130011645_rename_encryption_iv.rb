class RenameEncryptionIv < ActiveRecord::Migration[5.2]
  def change
    rename_column :links, :content_encryption_iv, :content_encryption_iv_base64
    rename_column :attachments, :file_encryption_iv, :file_encryption_iv_base64
    rename_column :imports, :file_encryption_iv, :file_encryption_iv_base64
  end
end
