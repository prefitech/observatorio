class CreatePeopleTags < ActiveRecord::Migration[5.0]
  def change
    create_table :people_tags, id: false do |t|
      t.timestamps             null: false
      t.uuid       :person_id, null: false
      t.uuid       :tag_id,    null: false
    end
    add_index :people_tags, :person_id
    add_index :people_tags, :tag_id
  end
end
