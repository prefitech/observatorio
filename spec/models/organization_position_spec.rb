describe OrganizationPosition do
  subject { FactoryBot.build(:organization_position) }

  it { should have_many(:organization_members)
                .class_name(OrganizationMember)
                .inverse_of(:position)
                .with_foreign_key("position_id")
                .dependent(:restrict_with_error) }

  it { should validate_presence_of(:name) }
  it { should validate_uniqueness_of(:name).case_insensitive }
end
