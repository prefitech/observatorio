describe EducationCourse do
  subject { FactoryBot.build(:education_course) }

  it { should have_many(:educations)
                .class_name(Education)
                .inverse_of(:course)
                .with_foreign_key("course_id")
                .dependent(:restrict_with_error) }

  it { should validate_presence_of(:name) }

  it { should validate_uniqueness_of(:name).case_insensitive }
end
