describe OrganizationMember do
  subject { FactoryBot.build(:organization_member) }

  it { should belong_to(:position) }
  it { should belong_to(:member) }

  it { should validate_presence_of(:member) }
  it { should validate_presence_of(:organization) }

  it "should validate duplicate" do
    organization = FactoryBot.create(:organization)
    department = FactoryBot.create(:organization_department, organization: organization)
    position = FactoryBot.create(:organization_position)
    existing = FactoryBot.create(:organization_member,
                                 position: position,
                                 organization: organization,
                                 department: department,
                                 from_date: Date.new(1990, 7, 3),
                                 to_date: Date.new(1997, 9, 1))
    subject = FactoryBot.build(:organization_member,
                               organization: existing.organization,
                               department: department,
                               member: existing.member,
                               from_date: existing.from_date,
                               to_date: existing.to_date,
                               position: existing.position)
    expect(subject).not_to be_valid
    subject.department = FactoryBot.create(:organization_department, organization: organization)
    expect(subject).to be_valid
    subject.department = existing.department
    subject.location = FactoryBot.create(:location)
    expect(subject).to be_valid
    subject.location = existing.location
    subject.from_date = Date.new(1987, 4, 8)
    expect(subject).not_to be_valid
    subject.to_date = Date.new(1989, 10, 4)
    expect(subject).to be_valid
  end

  it "should validate department belongs to organization" do
    subject.department = FactoryBot.create(:organization_department)
    expect(subject).not_to be_valid
    subject.department = subject.organization.departments.sample
    expect(subject).to be_valid
    subject.department = nil
    expect(subject).to be_valid
  end
end
