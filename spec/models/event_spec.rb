describe Event do
  subject { FactoryBot.build(:event) }

  it { should validate_presence_of(:name) }
  it { should validate_uniqueness_of(:name).case_insensitive }

  it { should have_many(:participants)
                .class_name(EventParticipant)
                .inverse_of(:event)
                .with_foreign_key("event_id")
                .dependent(:destroy) }
  it { should have_many(:tagged_events)
                .class_name(TaggedEvent)
                .inverse_of(:event)
                .with_foreign_key("event_id") }
  it { should have_many(:tags)
                .class_name(EventTag)
                .through(:tagged_events)
                .dependent(:destroy) }

  it { should have_many(:notes) }
  it { should have_many(:attachments) }
  it { should have_many(:links) }

  it "should validate presence of time zone if started_at or finished_at are set" do
    subject.time_zone = nil
    expect(subject).not_to be_valid
    subject.time_zone = random_tz
    expect(subject).to be_valid
    subject.started_at = nil
    subject.finished_at = nil
    subject.time_zone = nil
    expect(subject).to be_valid
    subject.finished_at = Time.now - 1.day
    expect(subject).not_to be_valid
    subject.time_zone = random_tz
    subject.approximate_dates = random_bool
    expect(subject).to be_valid
  end

  describe "#started_at_in_time_zone" do
    let(:event) { FactoryBot.build(:event, started_at: Time.now - 10.days, time_zone: random_tz) }
    subject { event.started_at_in_time_zone }
    it { should eq(event.started_at.in_time_zone(event.time_zone)) }
  end

  describe "#finished_at_in_time_zone" do
    let(:event) { FactoryBot.build(:event, finished_at: Time.now - 10.days, time_zone: random_tz) }
    subject { event.finished_at_in_time_zone }
    it { should eq(event.finished_at.in_time_zone(event.time_zone)) }
  end

  describe ".default_scope" do
    let(:event1) { FactoryBot.create(:event, name: "Event A") }
    let(:event2) { FactoryBot.create(:event, name: "Event Z") }
    let(:event3) { FactoryBot.create(:event, name: "Event B") }

    subject { described_class.all }

    it { should eq([event1, event3, event2]) }
  end
end
