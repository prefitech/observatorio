describe Link do
  subject { FactoryBot.build(:link) }

  it { should belong_to :linkable }

  it { should validate_presence_of :label }
  it { should validate_presence_of :url }
  it { should validate_presence_of :visited_at }
  it { should validate_uniqueness_of(:label)
                .scoped_to(:linkable_id,
                           :linkable_type)
                .case_insensitive }
  it { should validate_uniqueness_of(:url)
                .scoped_to(:linkable_id,
                           :linkable_type)
                .case_insensitive }

  context "does not ignore content" do
    before { allow(subject).to receive(:ignore_content).and_return(false) }
    it { should validate_presence_of :signature }
  end

  it "should validate signature" do
    subject = FactoryBot.build(:link, signature: "invalid")
    expect(subject).not_to be_valid
    subject.signature = subject.generate_signature
    expect(subject).to be_valid
  end

  describe "#save" do
    context "when linkable is a Relationship" do
      let(:relationship) { FactoryBot.create(:relationship) }
      context "on create" do
        it "should create link on inverse relationship" do
          link = FactoryBot.create(:link, linkable: relationship)
          inverse_link = Link.find_by(linkable: relationship.inverse_relationship)
          expect(inverse_link).to be_present
          expect(inverse_link.label).to eq(link.label)
          expect(inverse_link.url).to eq(link.url)
          expect(inverse_link.visited_at).to be_within(0.01).of(link.visited_at)
          expect(inverse_link.content.read).to eq(link.content.read)
          expect(inverse_link.content_size).to eq(link.content_size)
          expect(inverse_link.content_extension).to eq(link.content_extension)
          expect(inverse_link.content_encryption_iv_base64).to (
            eq(link.content_encryption_iv_base64)
          )
        end
      end
      context "on update" do
        let(:link) { FactoryBot.create(:link, linkable: relationship) }
        let(:new_label) { random_string }
        it "should update link on inverse relationship" do
          link.update_attributes!(label: new_label)
          inverse_link = Link.find_by(linkable: relationship.inverse_relationship)
          expect(inverse_link).to be_present
          expect(inverse_link.label).to eq(new_label)
        end
      end
      context "on destroy" do
        let!(:link) { FactoryBot.create(:link, linkable: relationship) }
        it "should destroy link on inverse relationship" do
          link.destroy!
          inverse_link = Link.find_by(linkable: relationship.inverse_relationship)
          expect(inverse_link).not_to be_present
        end
      end
    end
  end

  describe "#destroy" do
    context "when linkable is a Relationship" do
      let!(:relationship) { FactoryBot.create(:relationship) }
      let!(:link) { FactoryBot.create(:link, linkable: relationship) }
      subject { lambda { link.destroy! } }
      it { should change(Link, :count).from(2).to(0) }
    end
  end

  describe "#generate_signature" do
    let(:content) { File.new(File.join(Rails.root, "spec", "files", "meinhof.jpg")) }
    let(:expected_content) { File.new(File.join(Rails.root, "spec", "files", "meinhof.jpg")) }
    let(:visited_at) { random_datetime }
    let(:extension) { random_extension }
    let(:content_forbidden) { random_bool }
    let(:link) do
      FactoryBot.build(:link,
                       content_unencrypted: content,
                       content_forbidden: content_forbidden,
                       visited_at: visited_at,
                       content_extension: extension)
    end
    subject { link.generate_signature }
    it do
      should eq Digest::SHA512.base64digest(
        "#{link.url}|#{visited_at.to_i}|#{extension}|#{content_forbidden}|" \
        "#{Rails.application.secrets.secret_key_base}|#{expected_content.read}"
      )
    end
  end

  describe "#download_content" do
    let(:content) { File.new(File.join(Rails.root, "spec", "files", "meinhof.jpg")) }
    let(:expected_content) { File.new(File.join(Rails.root, "spec", "files", "meinhof.jpg")) }
    let(:link) { FactoryBot.build(:link, :no_content) }
    let(:extension) { ".ttf" }
    let(:visited_at) { random_datetime }
    subject { lambda { link.send(:download_content) } }
    before do
      allow(DOWNLOADER).to receive(:head)
                             .with(link.url)
                             .and_return(double(
                                extension: extension,
                                visited_at: visited_at,
                                success?: true
                             ))
      allow(DOWNLOADER).to receive(:get)
                             .with(link.url)
                             .and_return(double(
                                file: content,
                                extension: extension,
                                visited_at: visited_at
                             ))
    end
    it { should change(link, :content_extension).from(nil).to(".ttf") }
    it { should change(link, :content_size).from(nil).to(expected_content.size) }
    it { should change(link, :visited_at).from(nil).to(visited_at) }
    context "when content is forbidden" do
      before do
        allow(DOWNLOADER).to receive(:head)
                               .with(link.url)
                               .and_raise(Downloader::Forbidden)
      end
      it { should change(link, :content_forbidden).from(false).to(true) }
    end
    context "when content is HTML" do
      let(:pdf_content) { File.new(File.join(Rails.root, "spec", "files", "beethoven.html")) }
      let(:pdf_visited_at) { random_datetime }
      let(:extension) { ".html" }
      before do
        allow(HTML_TO_PDF).to receive(:convert)
                                .with(link.url)
                                .and_return(double(
                                   file: pdf_content,
                                   extension: ".pdf",
                                   visited_at: pdf_visited_at
                                ))
      end
      it { should change(link, :content_extension).from(nil).to(".pdf") }
      it { should change(link, :content_size).from(nil).to(pdf_content.size) }
      it { should change(link, :visited_at).from(nil).to(pdf_visited_at) }
      ["https://youtu.be/98271981",
       "https://youtube.com/watch?v=98271981",
       "https://www.youtube.fr/watch?v=98271981",
       "https://www.youtube.com/watch?v=98271981",
       "https://vimeo.com/200806635"].each do |url|
        context "and URL is #{url}" do
          let(:link) { FactoryBot.build(:link, :no_content, url: url) }
          it { should change(link, :content_forbidden).from(false).to(true) }
        end
      end

      context "and PDF convertion fails" do
        before do
          allow(HTML_TO_PDF).to receive(:convert)
                                  .with(link.url)
                                  .and_raise(HtmlToPdf::Error)
        end
        it { should change(link, :content_forbidden).from(false).to(true) }
      end
    end
  end

  describe "#content_exists" do
    let(:link) { FactoryBot.build(:link, :no_content) }
    subject { lambda { link.send(:content_exists) } }
    it { should change { link.errors[:url].present? }.from(false).to(true) }
    context "when content is forbidden" do
      before { link.content_forbidden = true }
      it { should_not change { link.errors[:url].present? }.from(false) }
    end
    context "when content is ignored" do
      before { link.ignore_content = true }
      it { should_not change { link.errors[:url].present? }.from(false) }
    end
  end
end
