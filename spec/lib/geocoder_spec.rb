describe Geocoder, :nominatim do
  let(:geocoder) { described_class.new }

  describe "#reverse" do
    let(:params) { { lat: random_lat, lon: random_lng, :"accept-language" => I18n.locale, format: "json" } }
    let(:response) { double("Nominatim response", success?: true, body: reverse_ok) }
    before { allow(described_class::NOMINATIM_API).to receive(:get)
                                                        .with("/reverse", anything)
                                                        .and_return(response) }
    it "should call Nomiatim API with given params" do
      expect(described_class::NOMINATIM_API).to receive(:get)
                                                  .with("/reverse", params)
                                                  .and_return(response)
      geocoder.reverse(params)
    end
    it "should parse response from Nominatim" do
      geocoded = geocoder.reverse(params)
      expect(geocoded[:lat]).to eq(params[:lat])
      expect(geocoded[:lng]).to eq(params[:lon])
      expect(geocoded[:address]["street"]).to eq("Place de la Défense")
      expect(geocoded[:address]["city"]).to eq("Puteaux")
      expect(geocoded[:geojson]).to eq({
        "type" => "Point",
        "coordinates" => [
          params[:lon],
          params[:lat]
        ]
      })
      expect(geocoded[:boundingbox]).to be_nil
    end
    context "when osm_id & osm_type given" do
      let(:params) { { osm_id: 19546127, osm_type: "W", :"accept-language" => I18n.locale, format: "json" } }
      it "should parse response for Nominatim response" do
        geocoded = geocoder.reverse(params)
        expect(geocoded[:lat]).to be_nil
        expect(geocoded[:lng]).to be_nil
        expect(geocoded[:address]["street"]).to eq("Place de la Défense")
        expect(geocoded[:address]["city"]).to eq("Puteaux")
        expect(geocoded[:geojson]).to eq(reverse_ok["geojson"])
        expect(geocoded[:boundingbox]).to eq(reverse_ok["boundingbox"])
      end
    end
    context "when geocoder has default params" do
      let(:geocoder) { described_class.new(format: "json") }
      let(:params) { { lat: random_lat, lon: random_lng, :"accept-language" => I18n.locale } }
      it "should call Nomiatim API with given params and default params" do
        expect(described_class::NOMINATIM_API).to receive(:get)
                                                    .with("/reverse", params.merge(format: "json"))
                                                    .and_return(response)
        geocoder.reverse(params)
      end
    end
    context "when given param lng" do
      let(:params) { { lat: random_lat, lng: random_lng, :"accept-language" => I18n.locale, format: "json" } }
      it "should call Nomiatim API with lon param" do
        cloned_params = params.clone
        expect(described_class::NOMINATIM_API).to receive(:get)
                                                    .with("/reverse", cloned_params.merge(lon: cloned_params.delete(:lng)))
                                                    .and_return(response)
        geocoder.reverse(params)
      end
    end
    context "when given param language" do
      let(:params) { { lat: random_lat, lon: random_lng, language: I18n.locale, format: "json" } }
      it "should call Nomiatim API with accept-language param" do
        cloned_params = params.clone
        expect(described_class::NOMINATIM_API).to receive(:get)
                                                    .with("/reverse", cloned_params.merge(:"accept-language" => cloned_params.delete(:language)))
                                                    .and_return(response)
        geocoder.reverse(params)
      end
    end
    { "node" => "N", "way" => "W", "relation" => "R" }.each do |key, value|
      context "when osm_type param is '#{key}'" do
        let(:params) { { osm_id: random_positive_int, osm_type: key, :"accept-language" => I18n.locale, format: "json" } }
        it "should call Nomiatim API with osm_type '#{value}'" do
          expect(described_class::NOMINATIM_API).to receive(:get)
                                                      .with("/reverse", params.merge(osm_type: value))
                                                      .and_return(response)
          geocoder.reverse(params)
        end
      end
    end
    context "when response addres has construction field" do
      let(:response) { double("Nominatim response", success?: true, body: reverse_ok_construction) }
      it "should parse construction field as street" do
        expect(geocoder.reverse(params)[:address]["street"]).to eq("Passerelle Marcelle Henry")
      end
    end
    context "when response addres has address26 field" do
      let(:response) { double("Nominatim response", success?: true, body: reverse_ok_address26) }
      it "should parse address26 field as street" do
        expect(geocoder.reverse(params)[:address]["street"]).to eq("Rue Ernest Flammarion")
      end
    end
    context "when response is not successfull" do
      let(:response) { double("Nominatim response", success?: false) }
      it "should raise error" do
        expect { geocoder.reverse(params) }.to raise_error(Geocoder::ServiceError)
      end
    end
    context "when response is not found" do
      let(:response) { double("Nominatim response", success?: true, body: reverse_not_found) }
      it "should raise error" do
        expect { geocoder.reverse(params) }.to raise_error(Geocoder::GeocodingError)
      end
    end
  end
end
