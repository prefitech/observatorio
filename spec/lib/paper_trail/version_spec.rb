describe PaperTrail::Version do
  class described_class::Vehicle < ApplicationRecord
    has_paper_trail
    belongs_to :brand
    belongs_to :category
  end
  class described_class::Brand < ApplicationRecord
    has_paper_trail
    belongs_to :company
  end
  class described_class::Company < ApplicationRecord
    has_paper_trail
  end
  class described_class::Category < ApplicationRecord
  end

  create_test_model_table described_class::Vehicle, id: :uuid do |t|
    t.text :model
    t.date :date
    t.datetime :datetime
    t.uuid :brand_id
    t.uuid :category_id
  end
  create_test_model_table described_class::Brand, id: :uuid do |t|
    t.text :name, null: false
    t.uuid :company_id
  end
  create_test_model_table described_class::Category, id: :uuid do |t|
    t.text :name, null: false
  end
  create_test_model_table described_class::Company, id: :uuid do |t|
    t.text :name, null: false
  end

  describe "#item_state" do
    context "on create" do
      it "should give db record" do
        brand = described_class::Brand.create!(name: "Brand name")
        vehicle = described_class::Vehicle.create!(model: "Vehicle model", brand: brand)
        vehicle_state = vehicle.versions.first.item_state(belongs_to: true)
        expect(vehicle_state.model).to eq("Vehicle model")
        expect(vehicle_state.brand.name).to eq("Brand name")
      end

      it "should give db record with reified association" do
        brand = described_class::Brand.create!(name: "Brand name")
        vehicle = described_class::Vehicle.create!(model: "Vehicle model", brand: brand)
        brand.update_attributes!(name: "Modified brand name")
        vehicle_state = vehicle.versions.first.item_state(belongs_to: true)
        expect(vehicle_state.model).to eq("Vehicle model")
        expect(vehicle_state.brand.name).to eq("Brand name")
      end

      it "should give db record with reified destroyed association" do
        brand = described_class::Brand.create!(name: "Brand name")
        vehicle = described_class::Vehicle.create!(model: "Vehicle model", brand: brand)
        brand.destroy!
        vehicle_state = vehicle.versions.first.item_state(belongs_to: true)
        expect(vehicle_state.model).to eq("Vehicle model")
        expect(vehicle_state.brand.name).to eq("Brand name")
      end

      it "should give db record with reified association of belongs_to" do
        company = described_class::Company.create!(name: "Company name")
        brand = described_class::Brand.create!(name: "Brand name", company: company)
        vehicle = described_class::Vehicle.create!(model: "Vehicle model", brand: brand)
        company.destroy!
        vehicle_state = vehicle.versions.first.item_state(
          belongs_to: true
        )
        expect(vehicle_state.model).to eq("Vehicle model")
        expect(vehicle_state.brand.name).to eq("Brand name")
        expect(vehicle_state.brand.company.name).to eq("Company name")
      end

      it "should give reified record with db association" do
        brand = described_class::Brand.create!(name: "Brand name")
        vehicle = described_class::Vehicle.create!(model: "Vehicle model", brand: brand)
        vehicle.update_attributes!(model: "Modified model")
        vehicle_state = vehicle.versions.first.item_state(belongs_to: true)
        expect(vehicle_state.model).to eq("Vehicle model")
        expect(vehicle_state.brand.name).to eq("Brand name")
      end

      it "should give reified record with reified association" do
        brand = described_class::Brand.create!(name: "Brand name")
        vehicle = described_class::Vehicle.create!(model: "Vehicle model", brand: brand)
        vehicle.update_attributes!(model: "Modified model")
        brand.update_attributes!(name: "Modified brand name")
        vehicle_state = vehicle.versions.first.item_state(belongs_to: true)
        expect(vehicle_state.model).to eq("Vehicle model")
        expect(vehicle_state.brand.name).to eq("Brand name")
      end
    end

    context "on update" do
      it "should give reified record" do
        brand = described_class::Brand.create!(name: "Brand name")
        vehicle = described_class::Vehicle.create!(model: "Vehicle model", brand: brand)
        vehicle.update_attributes!(model: "Modified model")
        brand.update_attributes!(name: "Modified brand name")
        vehicle_state = vehicle.versions.last.item_state(belongs_to: true)
        expect(vehicle_state.model).to eq("Vehicle model")
        expect(vehicle_state.brand.name).to eq("Brand name")
      end
    end

    context "on destroy" do
      it "should give reified record" do
        brand = described_class::Brand.create!(name: "Brand name")
        vehicle = described_class::Vehicle.create!(model: "Vehicle model", brand: brand)
        vehicle.destroy!
        brand.destroy!
        vehicle_state = vehicle.versions.last.item_state(belongs_to: true)
        expect(vehicle_state.model).to eq("Vehicle model")
        expect(vehicle_state.brand.name).to eq("Brand name")
      end
    end
  end

  describe "#converted_changeset" do
    context "when attribute is a date" do
      let(:vehicle) { described_class::Vehicle.create!(date: random_date) }
      subject { vehicle.versions.last.converted_changeset["date"] }
      it { should eq [nil, vehicle.date] }
    end

    context "when attribute is a datetime" do
      let(:vehicle) { described_class::Vehicle.create!(datetime: random_datetime) }
      subject { vehicle.versions.last.converted_changeset["datetime"] }
      it { should eq [nil, vehicle.datetime] }
    end

    context "when attribute is a string" do
      let(:vehicle) { described_class::Vehicle.create!(model: random_string) }
      subject { vehicle.versions.last.converted_changeset["model"] }
      it { should eq [nil, vehicle.model] }
    end

    context "when attribute is an association" do
      let(:brand) { described_class::Brand.create!(name: "New brand") }
      let(:vehicle) { described_class::Vehicle.create!(brand: brand) }
      subject { vehicle.versions.last.converted_changeset["brand_id"] }
      it { should eq [nil, brand] }
    end

    context "when attribute is an untracked association" do
      let(:category) { described_class::Category.create!(name: "Category name") }
      let(:vehicle) { described_class::Vehicle.create!(category: category) }
      subject { vehicle.versions.last.converted_changeset["category_id"] }
      it { should eq [nil, category] }
    end
  end
end
