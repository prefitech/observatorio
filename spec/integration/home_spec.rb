describe "home page" do
  it "should redirect to login" do
    get "/"
    expect(response).to redirect_to("/account/login")
  end
  context "when user is logged" do
    let(:user) { FactoryBot.create(:user, :confirmed) }
    before { sign_in(user) }
    it "should redirect to people index" do
      get "/"
      expect(response).to redirect_to("/people")
    end
  end
end
