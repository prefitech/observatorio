describe "education degrees" do
  let(:user) { FactoryBot.create(:user, :confirmed) }

  before do
    sign_in(user)
    PaperTrail.request.whodunnit = user
  end

  # EDUCATION DEGREES INDEX
  describe "GET /education_degrees" do
    let!(:suffix1) { "Facility" }
    let!(:suffix2) { "Nature" }

    let!(:education_degree1) { FactoryBot.create(:education_degree, name: "Education Degree A #{suffix1}") }
    let!(:education_degree2) { FactoryBot.create(:education_degree, name: "Education Degree B #{suffix1}") }
    let!(:education_degree3) { FactoryBot.create(:education_degree, name: "Education Degree C #{suffix2}") }
    let!(:education_degree4) { FactoryBot.create(:education_degree, name: "Education Degree D #{suffix2}") }
    let!(:education_degree5) { FactoryBot.create(:education_degree, name: "Education Degree E #{suffix2}") }
    let!(:education_degree6) { FactoryBot.create(:education_degree, name: "Education Degree F #{suffix2}") }
    let!(:education_degree7) { FactoryBot.create(:education_degree, name: "Education Degree G #{suffix2}") }
    let!(:education_degree8) { FactoryBot.create(:education_degree, name: "Education Degree H #{suffix2}") }
    let!(:education_degree9) { FactoryBot.create(:education_degree, name: "Education Degree I #{suffix2}") }
    let!(:education_degree10) { FactoryBot.create(:education_degree, name: "Education Degree J #{suffix2}") }
    let!(:education_degree11) { FactoryBot.create(:education_degree, name: "Education Degree K #{suffix2}") }
    let!(:education_degree12) { FactoryBot.create(:education_degree, name: "Education Degree L #{suffix2}") }

    before { authorize(:list, EducationDegree) }
    it "should list only 8 education degrees per page" do
      get "/education_degrees"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
      expect(response.body).to include(education_degree1.name)
      expect(response.body).to include(education_degree2.name)
      expect(response.body).to include(education_degree3.name)
      expect(response.body).to include(education_degree4.name)
      expect(response.body).to include(education_degree5.name)
      expect(response.body).to include(education_degree6.name)
      expect(response.body).to include(education_degree7.name)
      expect(response.body).to include(education_degree8.name)
      expect(response.body).not_to include(education_degree9.name)
      expect(response.body).not_to include(education_degree10.name)
      expect(response.body).not_to include(education_degree11.name)
      expect(response.body).not_to include(education_degree12.name)
    end
    it "should limit search results to 8 education degrees per page" do
      get "/education_degrees", params: { search: suffix2 }
      expect(response.body).to include(education_degree3.name)
      expect(response.body).to include(education_degree4.name)
      expect(response.body).to include(education_degree5.name)
      expect(response.body).to include(education_degree6.name)
      expect(response.body).to include(education_degree7.name)
      expect(response.body).to include(education_degree8.name)
      expect(response.body).to include(education_degree9.name)
      expect(response.body).to include(education_degree10.name)
      expect(response.body).not_to include(education_degree11.name)
      expect(response.body).not_to include(education_degree12.name)
    end
    it "should only return 2 education degrees" do
      get "/education_degrees", params: { search: suffix1 }
      expect(response.body).to include(education_degree1.name)
      expect(response.body).to include(education_degree2.name)
      expect(response.body).not_to include(education_degree3.name)
      expect(response.body).not_to include(education_degree4.name)
      expect(response.body).not_to include(education_degree5.name)
      expect(response.body).not_to include(education_degree6.name)
      expect(response.body).not_to include(education_degree7.name)
      expect(response.body).not_to include(education_degree8.name)
      expect(response.body).not_to include(education_degree9.name)
      expect(response.body).not_to include(education_degree10.name)
      expect(response.body).not_to include(education_degree11.name)
      expect(response.body).not_to include(education_degree12.name)
    end
    context "when user is not authorized to list education degrees" do
      before { unauthorize(:list, EducationDegree) }
      it "should not list education degrees" do
        get "/education_degrees", params: { query: education_degree1.name }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
        expect(response.body).not_to include(education_degree1.name)
        expect(response.body).not_to include(education_degree1.id)
      end
    end
  end

  # NEW EDUCATION DEGREE FORM
  describe "GET /education_degrees/new" do
    before { authorize :create, EducationDegree }
    it "should render new degree form" do
      get "/education_degrees/new"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when user is not authorized to create degrees" do
      before { unauthorize :create, EducationDegree }
      it "should not render new position form" do
        get "/education_degrees/new"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # CREATE EDUCATION DEGREE
  describe "POST /education_degrees" do
    let(:name) { random_string }
    before { authorize(:create, EducationDegree) }
    it "should create education degree" do
      expect {
        post "/education_degrees", params: {
          education_degree: {
            name: name
          }
        }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:create)
      }.to change(EducationDegree, :count).from(0).to(1)
      created_degree = EducationDegree.first
      expect(created_degree.name).to eq(name)
    end
    context "when validation fails" do
      it "should not create education degree" do
        expect {
          post "/education_degrees", params: {
            education_degree: {
              name: ""
            }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:form)
        }.not_to change(EducationDegree, :count)
      end
    end
    context "when user is not authorized to create education degrees" do
      before { unauthorize(:create, EducationDegree) }
      it "should not create education degree" do
        expect {
          post "/education_degrees", params: {
            education_degree: {
              name: name
            }
          }
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(EducationDegree, :count)
      end
    end
  end

  # SHOW EDUCATION DEGREE
  describe "GET /education_degrees/:id" do
    let(:education_degree) { FactoryBot.create(:education_degree) }
    let!(:educations) { FactoryBot.create_list(:education, rand(5..10), degree: education_degree) }
    before { authorize(:read, education_degree) }
    it "should show education degree" do
      get "/education_degrees/#{education_degree.id}"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:show)
    end
    context "when user is not authorized to read the education degree" do
      before { unauthorize(:read, education_degree) }
      it "should not show education degree" do
        get "/education_degrees/#{education_degree.id}"
        expect(response).to have_http_status(:forbidden)
      end
    end
  end

  # SHOW EDUCATION DEGREE'S HISTORY
  describe "GET /education_degrees/:id/history" do
    let!(:education_degree) { FactoryBot.create(:education_degree) }
    before { authorize :read, education_degree }
    it "should show education_degrees's changes history" do
      get "/education_degrees/#{education_degree.id}/history"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:history)
    end
    context "when user is not authorized to read education_degree" do
      before { unauthorize :read, education_degree }
      it "should not show education_degrees's changes history" do
        get "/education_degrees/#{education_degree.id}/history"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # EDIT EDUCATION DEGREE FORM
  describe "GET /education_degrees/:id/edit" do
    let(:education_degree) { FactoryBot.create(:education_degree) }
    before { authorize :update, education_degree }
    it "should render edit education_degree form" do
      get "/education_degrees/#{education_degree.id}/edit"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when user is not authorized to edit education_degree" do
      before { unauthorize :update, education_degree }
      it "should not render edit education_degree form" do
        get "/education_degrees/#{education_degree.id}/edit"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # UPDATE EDUCATION DEGREE
  describe "PUT /education_degrees" do
    let!(:education_degree) { FactoryBot.create(:education_degree) }
    let(:new_name) { random_string }
    before { authorize(:update, education_degree) }
    it "should update education degree" do
      put "/education_degrees/#{education_degree.id}", params: {
        education_degree: {
          name: new_name
        }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:update)
      education_degree.reload
      expect(education_degree.name).to eq(new_name)
    end
    context "when validation fails" do
      it "should not update education degree" do
        expect {
          put "/education_degrees/#{education_degree.id}", params: {
            education_degree: {
              name: ""
            }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:form)
        }.not_to change { education_degree.reload.name }
      end
    end
    context "when user is not authorized to update the education degree" do
      before { unauthorize(:update, education_degree) }
      it "should not update the education degree" do
        expect {
          put "/education_degrees/#{education_degree.id}", params: {
            education_degree: {
              name: new_name
            }
          }
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change { education_degree.reload.name }
      end
    end
  end

  # DESTROY EDUCATION DEGREE
  describe "DELETE /education_degrees/:id" do
    let!(:education_degree) { FactoryBot.create(:education_degree) }
    before { authorize(:delete, education_degree) }
    it "should delete education degree" do
      expect {
        delete "/education_degrees/#{education_degree.id}"
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:destroy)
      }.to change(EducationDegree, :count).from(1).to(0)
    end
    context "when dependent education" do
      before { FactoryBot.create(:education, degree: education_degree) }
      it "should not delete education degree" do
        expect {
          delete "/education_degrees/#{education_degree.id}"
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:error)
        }.not_to change(EducationDegree, :count)
      end
    end
    context "when user is not authorized to delete the education degree" do
      before { unauthorize(:delete, education_degree) }
      it "should not delete education degree" do
        expect {
          delete "/education_degrees/#{education_degree.id}"
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(EducationDegree, :count)
      end
    end
  end
end
