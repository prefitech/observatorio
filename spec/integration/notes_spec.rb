describe "notes" do
  let(:user) { FactoryBot.create(:user, :confirmed) }

  before do
    sign_in(user)
    PaperTrail.request.whodunnit = user
  end

  # INDEX NOTES
  describe "GET /notes" do
    let(:person) { FactoryBot.create(:person) }
    let!(:note1) { FactoryBot.create(:note, subject: person) }
    let!(:note2) { FactoryBot.create(:note, subject: person) }
    let!(:note3) { FactoryBot.create(:note) }
    before { authorize :read, person }
    it "should list notes" do
      get "/notes", params: {
        note: { subject_id: person.id, subject_type: person.class }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
      expect(response.body).to include(note1.body)
      expect(response.body).to include(note2.body)
      expect(response.body).not_to include(note3.body)
    end
    context "when no subject is given" do
      it "should respond bad request" do
        get "/notes"
        expect(response).to have_http_status(:bad_request)
        expect(response).to render_template(:bad_request)
      end
    end
    context "when user is not authorized to read subject" do
      before { unauthorize :read, person }
      it "should not list notes" do
        get "/notes", params: {
          note: { subject_id: person.id, subject_type: person.class }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # NEW NOTE FORM
  describe "GET /notes/new" do
    let(:person) { FactoryBot.create(:person) }
    before { authorize :read, person }
    it "should render new note form" do
      get "/notes/new", params: {
        note: { subject_id: person.id, subject_type: person.class }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when no subject given" do
      it "should respond bad request" do
        get "/notes/new"
        expect(response).to have_http_status(:bad_request)
        expect(response).to render_template(:bad_request)
      end
    end
    context "when user is not authorized to read subject" do
      before { unauthorize :read, person }
      it "should not render new note form" do
        get "/notes/new", params: {
          note: { subject_id: person.id, subject_type: person.class }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # CREATE NOTE
  describe "POST /notes" do
    let(:body) { random_string }
    let(:person) { FactoryBot.create(:person) }
    before { authorize(:read, person) }
    it "should create note" do
      expect {
        post "/notes", params: {
          note: {
            body: body,
            subject_id: person.id,
            subject_type: Person
          }
        }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:create)
      }.to change(Note, :count).from(0).to(1)
      created_note = Note.first
      expect(created_note.body).to eq(body)
      expect(created_note.subject).to eq(person)
      expect(created_note.author).to eq(user)
    end
    context "when validation fails" do
      it "should not create note" do
        expect {
          post "/notes", params: {
            note: {
              body: "",
              subject_id: person.id,
              subject_type: Person
            }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:form)
        }.not_to change(Note, :count)
      end
    end
    context "when user is not authorized to read subject" do
      before { unauthorize(:read, person) }
      it "should not create note" do
        expect {
          post "/notes", params: {
            note: {
              body: body,
              subject_id: person.id,
              subject_type: Person
            }
          }
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(Note, :count)
      end
    end
  end

  # SHOW NOTE
  describe "GET /notes/:id" do
    let!(:person) { FactoryBot.create(:person) }
    let!(:note) { FactoryBot.create(:note, subject: person) }
    before { authorize :read, person }
    it "should show note" do
      get "/notes/#{note.id}"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:show)
    end
    context "when user is not authorized to read subject" do
      before { unauthorize :read, person }
      it "should not show note" do
        get "/notes/#{note.id}"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # DESTROY NOTE
  describe "DELETE /notes/:id" do
    let!(:person) { FactoryBot.create(:person) }
    let!(:note) { FactoryBot.create(:note, subject: person, author: user) }
    before { authorize :read, person }
    it "should delete note" do
      expect {
        delete "/notes/#{note.id}"
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:destroy)
      }.to change(Note, :count).from(1).to(0)
    end
    context "when user is not the author of the note" do
      let!(:note) { FactoryBot.create(:note) }
      it "should respond forbidden" do
        expect {
          delete "/notes/#{note.id}"
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(Note, :count)
      end
    end
  end
end
