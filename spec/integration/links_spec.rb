describe "links" do
  let(:user) { FactoryBot.create(:user, :confirmed) }

  before do
    sign_in(user)
  end

  # LINKS INDEX
  describe "GET /links" do
    let(:person) { FactoryBot.create(:person) }
    let!(:link1) { FactoryBot.create(:link, linkable: person) }
    let!(:link2) { FactoryBot.create(:link, linkable: person) }
    let!(:link3) { FactoryBot.create(:link) }
    before { authorize(:read, person) }
    it "should list links" do
      get "/links", params: {
        link: {
          linkable_id: person.id,
          linkable_type: Person
        }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
      expect(response.body).to include(link1.label)
      expect(response.body).to include(link2.label)
      expect(response.body).not_to include(link3.label)
    end
    context "when linkable_id is missing" do
      it "should give error" do
        get "/links", params: { link: { linkable_type: link1.linkable_type } }
        expect(response).to have_http_status(:bad_request)
      end
    end
    context "when linkable_type is missing" do
      it "should give error" do
        get "/links", params: { link: { linkable_id: link1.linkable_id } }
        expect(response).to have_http_status(:bad_request)
      end
    end
    context "when user is not authorized to read links linkable" do
      before { unauthorize :read, person }
      it "should not list links" do
        get "/links", params: {
          link: {
            linkable_id: person.id,
            linkable_type: Person
          }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # NEW LINK FORM
  describe "GET /links/new" do
    let(:person) { FactoryBot.create(:person) }
    before { authorize :update, person }
    it "should render new link form" do
      get "/links/new", params: {
        link: {
          linkable_id: person.id,
          linkable_type: person.class
        }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when user is not authorized to update linkable" do
      before { unauthorize :update, person }
      it "should not render new link form" do
        get "/links/new", params: {
          link: {
            linkable_id: person.id,
            linkable_type: person.class
          }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # CREATE LINK
  describe "POST on /links" do
    let(:person) { FactoryBot.create(:person) }
    let(:url) { random_url }
    let(:label) { random_string }
    let(:description) { random_string }
    let(:visited_at) { random_datetime }
    let(:content_extension) { random_extension }
    let(:content_unencrypted) { random_string }
    let(:content_unencrypted_base64) { Base64.encode64(content_unencrypted) }
    let(:content_forbidden) { false }
    let(:ignore_content) { false }
    let(:signature) do
      Digest::SHA512.base64digest(
        "#{url}|#{visited_at.to_i}|" \
        "#{content_extension}|#{content_forbidden}|" \
        "#{Rails.application.secrets.secret_key_base}|" \
        "#{content_unencrypted}"
      )
    end
    before { authorize :update, person }
    it "should create link" do
      expect {
        post "/links", params: {
          link: {
            url: url,
            label: label,
            signature: signature,
            visited_at: visited_at,
            description: description,
            linkable_id: person.id,
            linkable_type: person.class,
            ignore_content: ignore_content,
            content_forbidden: content_forbidden,
            content_extension: content_extension,
            content_unencrypted_base64: content_unencrypted_base64
          }
        }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:create)
      }.to change(Link, :count).from(0).to(1)
      created = Link.last
      expect(created.url).to eq(url)
      expect(created.label).to eq(label)
      expect(created.visited_at).to be_within(0.1).of(visited_at)
      expect(created.linkable).to eq(person)
      expect(created.description).to eq(description)
      expect(created.content_extension).to eq(content_extension)
      expect(created.content_decrypted.read).to eq(content_unencrypted)
    end
    context "when content is forbidden" do
      let(:content_extension) { ".html" }
      let(:content_unencrypted) { nil }
      let(:content_forbidden) { true }
      it "should create link" do
        expect {
          post "/links", params: {
            link: {
              url: url,
              label: label,
              signature: signature,
              visited_at: visited_at,
              linkable_id: person.id,
              linkable_type: person.class,
              ignore_content: ignore_content,
              content_forbidden: content_forbidden,
              content_extension: content_extension
            }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:create)
        }.to change(Link, :count).from(0).to(1)
        created = Link.last
        expect(created.url).to eq(url)
        expect(created.label).to eq(label)
        expect(created.visited_at).to be_within(0.1).of(visited_at)
        expect(created.linkable).to eq(person)
        expect(created.content_extension).to eq(content_extension)
        expect(created.content_decrypted).to be_nil
      end
    end
    context "when content is ignored" do
      let(:content_extension) { ".html" }
      let(:content_unencrypted) { nil }
      let(:ignore_content) { true }
      it "should create link" do
        expect {
          post "/links", params: {
            link: {
              url: url,
              label: label,
              signature: signature,
              visited_at: visited_at,
              linkable_id: person.id,
              linkable_type: person.class,
              ignore_content: ignore_content,
              content_forbidden: content_forbidden,
              content_extension: content_extension
            }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:create)
        }.to change(Link, :count).from(0).to(1)
        created = Link.last
        expect(created.url).to eq(url)
        expect(created.label).to eq(label)
        expect(created.visited_at).to be_within(0.1).of(visited_at)
        expect(created.linkable).to eq(person)
        expect(created.content_extension).to eq(content_extension)
        expect(created.content_decrypted).to be_nil
      end
    end
    context "when validation fails" do
      let(:signature) { random_string }
      it "should not create link" do
        expect {
          post "/links", params: {
            link: {
              url: url,
              label: label,
              signature: signature,
              visited_at: visited_at,
              linkable_id: person.id,
              linkable_type: person.class,
              content_extension: content_extension,
              content_unencrypted_base64: content_unencrypted_base64
            }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:form)
        }.not_to change(Link, :count)
      end
    end
    context "when user is not authorized to update person" do
      before { unauthorize :update, person }
      it "should not create link" do
        expect {
          post "/links", params: {
            link: {
              url: url,
              label: label,
              signature: signature,
              visited_at: visited_at,
              linkable_id: person.id,
              linkable_type: person.class,
              content_extension: content_extension,
              content_unencrypted_base64: content_unencrypted_base64
            }
          }
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(Link, :count)
      end
    end
  end

  # SHOW LINK
  describe "GET /links/:id" do
    let(:person) { FactoryBot.create(:person) }
    let(:link) { FactoryBot.create(:link, linkable: person) }
    before { authorize :read, link }
    it "should show link" do
      get "/links/#{link.id}", params: {
        origin: {
          id: person.id,
          controller: "people"
        }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:show)
    end
    it "should respond link as json" do
      get "/links/#{link.id}.json", params: {
        origin: {
          id: person.id,
          controller: "people"
        }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:show)
    end
    [:html, :pdf, :image, :video, :audio].each do |type|
      context "when content is #{type}" do
        let(:link) { FactoryBot.create(:link, type, linkable: person) }
        it "should show link" do
          get "/links/#{link.id}", params: {
            origin: {
              id: person.id,
              controller: "people"
            }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:show)
        end
      end
    end
  end

  # LINK DOWNLOAD
  describe "GET /links/:id/download" do
    let(:link) { FactoryBot.create(:link, :html) }
    before { authorize(:read, link.linkable) }
    it "should show link content" do
      get "/links/#{link.id}/download"
      expect(response).to have_http_status(:ok)
    end
    context "when content is not a HTML page" do
      let(:link) { FactoryBot.create(:link) }
      it "should show link content" do
        get "/links/#{link.id}/download"
        expect(response).to have_http_status(:ok)
      end
    end
    context "when user is not authorized to read link's linkable" do
      before { unauthorize(:read, link.linkable) }
      it "should redirect to home page" do
        get "/links/#{link.id}/download"
        expect(response).to have_http_status(:forbidden)
        expect(response.body).not_to include(link.url)
      end
    end
  end

  # SHOW LINK'S HISTORY
  describe "GET /links/:id/history" do
    let!(:person) { FactoryBot.create(:person) }
    let!(:link) { FactoryBot.create(:link, linkable: person) }
    before { authorize :read, person }
    it "should show links's changes history" do
      get "/links/#{link.id}/history"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:history)
    end
    context "when user is not authorized to read person" do
      before { unauthorize :read, person }
      it "should not show links's changes history" do
        get "/links/#{link.id}/history"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # EDIT LINK FORM
  describe "GET /links/:id/edit" do
    let(:link) { FactoryBot.create(:link) }
    before { authorize :update, link.linkable }
    it "should render edit link form" do
      get "/links/#{link.id}/edit"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when user is not authorized to update linkable" do
      before { unauthorize :update, link.linkable }
      it "should not render edit link form" do
        get "/links/#{link.id}/edit"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # UPDATE LINK
  describe "PUT /links/:id" do
    let(:link) { FactoryBot.create(:link) }
    let(:new_label) { random_string }
    let(:new_description) { random_string }
    before { authorize(:update, link.linkable) }
    it "should update link" do
      put "/links/#{link.id}", params: {
        link: {
          label: new_label,
          description: new_description
        }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:update)
      link.reload
      expect(link.label).to eq(new_label)
      expect(link.description).to eq(new_description)
    end
    context "when validation fails" do
      it "shoud not update link" do
        expect {
          put "/links/#{link.id}", params: {
            link: {
              label: ""
            }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:form)
        }.not_to change { link.reload.label }
      end
    end
    context "when user is not authorized to update link's linkable" do
      before { unauthorize(:update, link.linkable) }
      it "should not authorize" do
        expect {
          put "/links/#{link.id}", params: {
            person: {
              label: new_label
            }
          }
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change { link.reload.label }
      end
    end
  end

  # DESTROY LINK
  describe "DELETE /links/:id" do
    let!(:link) { FactoryBot.create(:link) }
    before { authorize(:update, link.linkable) }
    it "should delete link" do
      expect {
        delete "/links/#{link.id}"
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:destroy)
      }.to change(Link, :count).from(1).to(0)
    end
    context "when user is not authorized to update link's linkable" do
      before { unauthorize(:update, link.linkable) }
      it "should not delete link" do
        expect {
          delete "/links/#{link.id}"
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(Link, :count)
      end
    end
  end
end
