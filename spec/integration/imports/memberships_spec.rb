describe "imports memberships" do
  let(:user) { FactoryBot.create(:user, :confirmed) }

  before do
    sign_in(user)
    PaperTrail.request.whodunnit = user
  end

  # IMPORTS INDEX
  describe "GET /imports/memberships" do
    let!(:imports) { FactoryBot.create_list(:memberships_import, rand(1..3)) }
    let(:import) { imports.sample }
    before do
      authorize(:list, MembershipsImport)
      authorize(:read, import)
    end
    it "should list imports" do
      get "/imports/memberships"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
      expect(response.body).to include(import.display_name)
    end
    context "when user is not authorized to list imports" do
      before { unauthorize(:list, MembershipsImport) }
      it "should not list imports" do
        get "/imports/memberships"
        expect(response).to have_http_status(:forbidden)
        expect(response.body).not_to include(import.display_name)
      end
    end
  end

  # SHOW IMPORT
  describe "GET /imports/memberships/:id" do
    let!(:import) { FactoryBot.create(:memberships_import) }
    before { authorize(:read, import) }
    it "should show import" do
      get "/imports/memberships/#{import.id}"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:show)
      expect(response.body).to include(import.display_name)
    end
    context "when user is not authorized to read the import" do
      before { unauthorize(:read, import) }
      it "should redirect to home page" do
        get "/imports/memberships/#{import.id}"
        expect(response).to have_http_status(:forbidden)
        expect(response.body).not_to include(import.display_name)
      end
    end
  end

  # DOWNLOAD IMPORT FILE
  describe "GET /imports/memberships/:id/download" do
    let!(:import) { FactoryBot.create(:memberships_import) }
    before { authorize(:read, import) }
    it "should stream import file" do
      get "/imports/memberships/#{import.id}/download"
      expect(response).to have_http_status(:ok)
    end
    context "when user is not authorized to read the import" do
      before { unauthorize(:read, import) }
      it "should redirect to home page" do
        get "/imports/memberships/#{import.id}/download"
        expect(response).to have_http_status(:forbidden)
      end
    end
  end

  # CREATE IMPORT
  describe "POST /imports/memberships" do
    let(:file) do
      Rack::Test::UploadedFile.new(
        File.join(Rails.root, "spec", "files", "memberships_import_ok.ods"),
        "application/vnd.oasis.opendocument.spreadsheet"
      )
    end
    before { authorize(:create, Import) }
    it "should create import" do
      expect {
        post "/imports/memberships", params: {
          import: { file_unencrypted: file }
        }
      }.to change(Import, :count).from(0).to(1)
      created_import = Import.first
      expect(response).to redirect_to("/imports/memberships/#{created_import.id}")
    end
    context "when validation fails" do
      let(:file) do
        Rack::Test::UploadedFile.new(
          File.join(Rails.root, "spec", "files", "memberships_import_missing_headers.ods"),
          "application/vnd.oasis.opendocument.spreadsheet"
        )
      end
      it "should not create import" do
        expect {
          post "/imports/memberships", params: {
            import: { file_unencrypted: file }
          }
          expect(response).to redirect_to("/imports/memberships")
        }.not_to change(Import, :count)
      end
    end
  end
end
