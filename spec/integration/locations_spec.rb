describe "locations", :nominatim do
  let(:user) { FactoryBot.create(:user, :confirmed) }

  before do
    sign_in(user)
    PaperTrail.request.whodunnit = user
  end

  # SHOW LOCATION
  describe "GET /locations/:id" do
    let!(:location) { FactoryBot.create(:location) }
    before { authorize(:read, location) }
    it "should render location attributes" do
      get "/locations/#{location.id}.json"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:show)
    end
    context "when user is not authorized to read location" do
      before { unauthorize(:read, location) }
      it "should not render location" do
        get "/locations/#{location.id}.json"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # REVERSE GEOCODE LOCATION
  describe "POST /locations/reverse_geocode" do
    let(:nominatim_response) { double("nominatim_response", success?: true, body: reverse_ok) }
    before do
      authorize(:create, Location)
      authorize(:read, Location)
    end
    context "when osm_id/osm_type provided" do
      let(:osm_id) { rand(500..100000).to_s }
      let(:osm_type) { ["W", "N", "R"].sample }
      context "and location exists in database" do
        let!(:location) do
          FactoryBot.create(:location, osm_id: osm_id, osm_type:
                            osm_type, lat: nil, lng: nil)
        end
        it "should render location from database" do
          expect(GEOCODER).not_to receive(:reverse)
          expect {
            post "/locations/reverse_geocode", params: {
              location: { osm_id: osm_id, osm_type: osm_type }
            }
            expect(response).to have_http_status(:ok)
            expect(response).to render_template(:show)
            expect(Oj.load(response.body)["id"]).to eq(location.id)
          }.not_to change(Location, :count)
        end
      end
      context "and location doest not exist in database" do
        before do
          allow(Geocoder::NOMINATIM_API).to receive(:get)
                                              .with("/reverse", {
                                                osm_id: osm_id,
                                                osm_type: osm_type,
                                                :"accept-language" => I18n.locale,
                                                addressdetails: 1,
                                                polygon_geojson: 1,
                                                format: "json"
                                              })
                                              .and_return(nominatim_response)
        end
        it "should reverse geocode location" do
          expect {
            post "/locations/reverse_geocode", params: {
              location: {
                osm_id: osm_id, osm_type: osm_type
              }
            }
            expect(response).to have_http_status(:ok)
            expect(response).to render_template(:show)
          }.to change(Location, :count).from(0).to(1)
          created_location = Location.first
          expect(Oj.load(response.body)["id"]).to eq(created_location.id)
        end
      end
    end
    context "when lat/lng provided" do
      let(:lat) { random_lat }
      let(:lng) { random_lng }
      context "and location exists in database" do
        let!(:location) { FactoryBot.create(:location, lat: lat, lng: lng) }
        it "should render location from database" do
          expect(GEOCODER).not_to receive(:reverse)
          expect {
            post "/locations/reverse_geocode", params: {
              location: { lat: lat, lng: lng }
            }
            expect(response).to have_http_status(:ok)
            expect(response).to render_template(:show)
            expect(Oj.load(response.body)["id"]).to eq(location.id)
          }.not_to change(Location, :count)
        end
      end
      context "and location does not exist in database" do
        before do
          allow(Geocoder::NOMINATIM_API).to receive(:get)
                                              .with("/reverse", {
                                                lat: lat.to_s,
                                                lon: lng.to_s,
                                                :"accept-language" => I18n.locale,
                                                addressdetails: 1,
                                                polygon_geojson: 1,
                                                format: "json"
                                              })
                                              .and_return(nominatim_response)
        end
        it "should reverse geocode location" do
          expect {
            post "/locations/reverse_geocode", params: {
              location: {
                lat: lat, lng: lng
              }
            }
            expect(response).to have_http_status(:ok)
            expect(response).to render_template(:show)
          }.to change(Location, :count).from(0).to(1)
          created_location = Location.first
          expect(Oj.load(response.body)["id"]).to eq(created_location.id)
        end
      end
    end
    context "when bad coordinates given" do
      let(:lat) { random_lat }
      let(:lng) { random_lng }
      let(:nominatim_response) { double("nominatim_response", success?: true, body: reverse_not_found) }
      before do
        allow(Geocoder::NOMINATIM_API).to receive(:get)
                                            .and_return(nominatim_response)
      end
      it "should return bad request error" do
        expect {
          post "/locations/reverse_geocode", params: {
            location: { lat: lat, lng: lng }
          }
          expect(response).to have_http_status(:bad_request)
          expect(response).to render_template(:bad_request)
        }.not_to change(Location, :count)
      end
    end
  end
end
