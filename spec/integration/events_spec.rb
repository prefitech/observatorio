describe "events" do
  let(:user) { FactoryBot.create(:user, :confirmed) }

  before do
    sign_in(user)
    PaperTrail.request.whodunnit = user
  end

  # EVENTS INDEX
  describe "GET /events" do
    let!(:suffix1) { "Daily" }
    let!(:suffix2) { "Weekend" }

    let!(:event1) { FactoryBot.create(:event, name: "Event A #{suffix1}") }
    let!(:event2) { FactoryBot.create(:event, name: "Event B #{suffix1}") }
    let!(:event3) { FactoryBot.create(:event, name: "Event C #{suffix2}") }
    let!(:event4) { FactoryBot.create(:event, name: "Event D #{suffix2}") }
    let!(:event5) { FactoryBot.create(:event, name: "Event E #{suffix2}") }
    let!(:event6) { FactoryBot.create(:event, name: "Event F #{suffix2}") }
    let!(:event7) { FactoryBot.create(:event, name: "Event G #{suffix2}") }
    let!(:event8) { FactoryBot.create(:event, name: "Event H #{suffix2}") }
    let!(:event9) { FactoryBot.create(:event, name: "Event I #{suffix2}") }
    let!(:event10) { FactoryBot.create(:event, name: "Event J #{suffix2}") }
    let!(:event11) { FactoryBot.create(:event, name: "Event K #{suffix2}") }
    let!(:event12) { FactoryBot.create(:event, name: "Event L #{suffix2}") }

    before { authorize(:list, Event) }
    it "should list only 8 events per page" do
      get "/events"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
      expect(response.body).to include(event1.name)
      expect(response.body).to include(event2.name)
      expect(response.body).to include(event3.name)
      expect(response.body).to include(event4.name)
      expect(response.body).to include(event5.name)
      expect(response.body).to include(event6.name)
      expect(response.body).to include(event7.name)
      expect(response.body).to include(event8.name)
      expect(response.body).not_to include(event9.name)
      expect(response.body).not_to include(event10.name)
      expect(response.body).not_to include(event11.name)
      expect(response.body).not_to include(event12.name)
    end
    it "should limit search results to 8 events per page" do
      get "/events", params: { search: suffix2 }
      expect(response.body).to include(event3.name)
      expect(response.body).to include(event4.name)
      expect(response.body).to include(event5.name)
      expect(response.body).to include(event6.name)
      expect(response.body).to include(event7.name)
      expect(response.body).to include(event8.name)
      expect(response.body).to include(event9.name)
      expect(response.body).to include(event10.name)
      expect(response.body).not_to include(event11.name)
      expect(response.body).not_to include(event12.name)
    end
    it "should only return 2 events" do
      get "/events", params: { search: suffix1 }
      expect(response.body).to include(event1.name)
      expect(response.body).to include(event2.name)
      expect(response.body).not_to include(event3.name)
      expect(response.body).not_to include(event4.name)
      expect(response.body).not_to include(event5.name)
      expect(response.body).not_to include(event6.name)
      expect(response.body).not_to include(event7.name)
      expect(response.body).not_to include(event8.name)
      expect(response.body).not_to include(event9.name)
      expect(response.body).not_to include(event10.name)
      expect(response.body).not_to include(event11.name)
      expect(response.body).not_to include(event12.name)
    end
    context "when user is not authorized to list events" do
      before { unauthorize(:list, Event) }
      it "should not list events" do
        get "/events", params: { search: suffix1 }
        expect(response).to have_http_status(:forbidden)
        expect(response.body).not_to include(event1.name)
      end
    end
  end

  # NEW EVENT
  describe "GET /events/new" do
    before { authorize(:create, Event) }
    it "should render new person form" do
      get "/events/new"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when user is not authorized to create events" do
      before { unauthorize(:create, Event) }
      it "should not render new person form" do
        get "/events/new"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # CREATE EVENT
  describe "POST /events" do
    let(:name) { random_string }
    let(:started_at) { Time.now - 3.days }
    let(:finished_at) { Time.now - 3.days }
    let(:time_zone) { random_tz }
    let(:approximate_dates) { random_bool }
    let(:location) { FactoryBot.create(:location) }
    let(:tag1) { FactoryBot.create(:tag, :event) }
    let(:tag2) { FactoryBot.create(:tag, :event) }
    before { authorize(:create, Event) }
    it "should create event" do
      expect {
        post "/events", params: {
          event: {
            name: name,
            started_at: started_at,
            finished_at: finished_at,
            time_zone: time_zone,
            approximate_dates: approximate_dates,
            location_id: location.id,
            tag_ids: [tag1.id, tag2.id]
          }
        }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:create)
      }.to change(Event, :count).from(0).to(1)
      created_event = Event.first
      expect(created_event.name).to eq(name)
      expect(created_event.started_at).to be_within(1.minute).of(started_at)
      expect(created_event.finished_at).to be_within(1.minute).of(finished_at)
      expect(created_event.time_zone).to eq(time_zone)
      expect(created_event.location).to eq(location)
      expect(created_event.tag_ids).to contain_exactly(tag1.id, tag2.id)
    end
    context "when validation fails" do
      it "should not create event" do
        expect {
          post "/events", params: {
            event: {
              name: ""
            }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:form)
        }.not_to change(Event, :count)
      end
    end
    context "when user is not authorized to create events" do
      before { unauthorize(:create, Event) }
      it "should not authorize" do
        expect {
          post "/events", params: {
            event: {
              name: name
            }
          }
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(Event, :count)
      end
    end
  end

  # SHOW EVENT
  describe "GET /events/:id" do
    let!(:event) { FactoryBot.create(:event) }
    before { authorize(:read, event) }
    it "should show event" do
      get "/events/#{event.id}"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:show)
      expect(response.body).to include(event.name)
    end
    context "when user is not authorized to read the event" do
      before { unauthorize(:read, event) }
      it "should respond forbidden" do
        get "/events/#{event.id}"
        expect(response).to have_http_status(:forbidden)
        expect(response.body).not_to include(event.name)
      end
    end
  end

  # SHOW EVENT'S HISTORY
  describe "GET /events/:id/history" do
    let!(:event) { FactoryBot.create(:event) }
    before { authorize :read, event }
    it "should show events's changes history" do
      get "/events/#{event.id}/history"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:history)
    end
    context "when user is not authorized to read event" do
      before { unauthorize :read, event }
      it "should not show events's changes history" do
        get "/events/#{event.id}/history"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # EDIT EVENT
  describe "GET /events/:id/edit" do
    let!(:event) { FactoryBot.create(:event) }
    before { authorize(:update, event) }
    it "should show edit events form" do
      get "/events/#{event.id}/edit"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when user is not authorized to edit the event" do
      before { unauthorize(:update, event) }
      it "should not show event edit form" do
        get "/events/#{event.id}/edit"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # UPDATE EVENT
  describe "PUT /events/:id" do
    let(:tag) { FactoryBot.create(:tag, :event) }
    let(:new_tag) { FactoryBot.create(:tag, :event) }
    let!(:event) { FactoryBot.create(:event, tag_ids: [tag.id]) }
    let(:new_name) { random_string }
    let(:new_started_at) { Time.now - 6.days }
    let(:new_finished_at) { Time.now - 4.days }
    let(:new_time_zone) { random_tz }
    let(:new_approzimate_dates) { random_bool }
    let(:new_location) { FactoryBot.create(:location) }
    before { authorize(:update, event) }
    it "should update event" do
      put "/events/#{event.id}", params: {
        event: {
          name: new_name,
          started_at: new_started_at,
          finished_at: new_finished_at,
          approximate_dates: new_approzimate_dates,
          time_zone: new_time_zone,
          tag_ids: [new_tag.id],
          location_id: new_location.id
        }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:update)
      event.reload
      expect(event.name).to eq(new_name)
      expect(event.started_at).to be_within(1.minute).of(new_started_at)
      expect(event.finished_at).to be_within(1.minute).of(new_finished_at)
      expect(event.approximate_dates).to eq(new_approzimate_dates)
      expect(event.location).to eq(new_location)
      expect(event.tag_ids).to contain_exactly(new_tag.id)
    end
    context "when validation fails" do
      it "shoud not update event" do
        expect {
          put "/events/#{event.id}", params: {
            event: {
              name: ""
            }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:form)
        }.not_to change { event.reload.name }
      end
    end
    context "when user is not authorized to update the event" do
      before { unauthorize(:update, event) }
      it "should not authorize" do
        expect {
          put "/events/#{event.id}", params: {
            event: {
              name: new_name
            }
          }
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change { event.reload.name }
      end
    end
  end

  # READ EVENT'S DESCRIPTION
  describe "GET /events/:id/description" do
    let(:event) { FactoryBot.create(:event) }
    before { authorize(:read, event) }
    it "should show event's description" do
      get "/events/#{event.id}/description"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:description)
    end
    context "when user is not authorized to update the event" do
      before { unauthorize(:read, event) }
      it "should not show event's description" do
        get "/events/#{event.id}/description"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # EDIT EVENTS'S DESCRIPTION
  describe "GET /events/:id/description/edit" do
    let(:event) { FactoryBot.create(:event) }
    before { authorize(:update, event) }
    it "should show event's edit description form" do
      get "/events/#{event.id}/description/edit"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form_description)
    end
    context "when user is not authorized to update the event" do
      before { unauthorize(:update, event) }
      it "should not show event's edit description form" do
        get "/events/#{event.id}/description/edit"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # UPDATE EVENT'S DESCRIPTION
  describe "PUT /events/:id/description" do
    let(:event) { FactoryBot.create(:event) }
    let(:new_description) { random_string }
    before { authorize(:update, event) }
    it "should update description" do
      expect {
        put "/events/#{event.id}/description", params: {
          event: { description: new_description }
        }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:update_description)
      }.to change { event.reload.description }.to(new_description)
    end
    context "when user is not authorized to update the event" do
      before { unauthorize(:update, event) }
      it "should not authorize" do
        expect {
          put "/events/#{event.id}/description"
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change { event.reload.description }
      end
    end
  end

  # DESTROY EVENT
  describe "DELETE /events/:id" do
    let!(:event) { FactoryBot.create(:event) }
    before { authorize(:delete, event) }
    it "should delete event" do
      expect {
        delete "/events/#{event.id}"
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:destroy)
      }.to change(Event, :count).from(1).to(0)
    end
    context "when user is not authorized to destroy the event" do
      before { unauthorize(:delete, event) }
      it "should not delete event" do
        expect {
          delete "/events/#{event.id}"
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(Event, :count)
      end
    end
  end
end
