describe "relationships" do
  let(:user) { FactoryBot.create(:user, :confirmed) }

  before do
    sign_in(user)
    PaperTrail.request.whodunnit = user
  end

  # RELATIONSHIPS INDEX
  describe "GET /relationships" do
    let!(:person) { FactoryBot.create(:person) }
    let!(:relationship1) { FactoryBot.create(:relationship, owner: person) }
    let!(:relationship2) { FactoryBot.create(:relationship, owner: person) }
    let!(:relationship3) { FactoryBot.create(:relationship) }
    before { authorize :read, person }
    it "should list relationships" do
      get "/relationships", params: {
        relationship: { owner_id: person.id }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
    end
    context "when owner_id is missing" do
      it "should give error" do
        get "/relationships"
        expect(response).to have_http_status(:bad_request)
        expect(response).to render_template(:bad_request)
      end
    end
    context "when user is not authorized to read relationship's owner" do
      before { unauthorize :read, person }
      it "should not list relationships" do
        get "/relationships", params: {
          relationship: { owner_id: person.id }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # NEW RELATIONSHIP FORM
  describe "GET /relationships/new" do
    let(:owner) { FactoryBot.create(:person) }
    before { authorize :update, owner }
    it "should render new relationship form" do
      get "/relationships/new", params: {
        relationship: { owner_id: owner.id }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when user is not authorized to update owner" do
      before { unauthorize :update, owner }
      it "should not render new relationship form" do
        get "/relationships/new", params: {
          relationship: { owner_id: owner.id }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # CREATE RELATIONSHIP
  describe "POST /relationships" do
    let(:owner) { FactoryBot.create(:person) }
    let(:with) { FactoryBot.create(:person) }
    before do
      authorize :update, owner
      authorize :update, with
    end
    it "should create relationship" do
      expect {
        post "/relationships", params: {
          relationship: {
            owner_id: owner.id,
            with_id: with.id,
            degree: Relationship::DEGREES.sample
          }
        }
      }.to change(Relationship, :count).from(0).to(2)
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:create)
      relationship = Relationship.where(owner_id: owner.id).first
      expect(relationship.with).to eq(with)
      inverse = Relationship.where(owner: with.id).first
      expect(inverse.with).to eq(owner)
    end
    context "when validation fails" do
      it "should render form" do
        expect {
          post "/relationships", params: {
            relationship: {
              owner_id: owner.id,
              with_id: with.id
            }
          }
        }.not_to change(Relationship, :count)
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:form)
      end
    end
    context "when user is not authorized to update owner" do
      before { unauthorize :update, owner }
      it "should not authorize creating relationship" do
        expect {
          post "/relationships", params: {
            relationship: {
              owner_id: owner.id,
              with_id: with.id,
              degree: Relationship::DEGREES.sample
            }
          }
        }.not_to change(Relationship, :count)
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
    context "when user is not authorized to update with" do
      before { unauthorize :update, with }
      it "should not authorize creating relationship" do
        expect {
          post "/relationships", params: {
            relationship: {
              owner_id: owner.id,
              with_id: with.id,
              degree: Relationship::DEGREES.sample
            }
          }
        }.not_to change(Relationship, :count)
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # SHOW RELATIONSHIP
  describe "GET in /relationships/:id" do
    let(:relationship) { FactoryBot.create(:relationship) }
    before do
      authorize :read, relationship.owner
      authorize :read, relationship.with
    end
    it "should render relationship" do
      get "/relationships/#{relationship.id}"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:show)
    end
    context "when user is not authorized to read owner" do
      before { unauthorize :read, relationship.owner }
      it "should not render relationship" do
        get "/relationships/#{relationship.id}"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
    context "when user is not authorized to read with" do
      before { unauthorize :read, relationship.with }
      it "should not render relationship" do
        get "/relationships/#{relationship.id}"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # EDIT RELATIONSHIP FORM
  describe "GET /relationships/:id/edit" do
    let(:relationship) { FactoryBot.create(:relationship) }
    before do
      authorize :update, relationship.owner
      authorize :update, relationship.with
    end
    it "should render edit relationship form" do
      get "/relationships/#{relationship.id}/edit"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when user is not authorized to update owner" do
      before { unauthorize :update, relationship.owner }
      it "should not render edit relationship form" do
        get "/relationships/#{relationship.id}/edit"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
    context "when user is not authorized to update with" do
      before { unauthorize :update, relationship.with }
      it "should not render edit relationship form" do
        get "/relationships/#{relationship.id}/edit"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # UPDATE RELATIONSHIP
  describe "PUT /relationships/:id" do
    let(:relationship) { FactoryBot.create(:relationship) }
    let(:new_degree) { (Relationship::DEGREES - [relationship.degree]).sample }
    before do
      authorize :update, relationship.owner
      authorize :update, relationship.with
    end
    it "should update relationship" do
      put "/relationships/#{relationship.id}", params: {
        relationship: { degree: new_degree }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:update)
      relationship.reload
      expect(relationship.degree).to eq(new_degree)
    end
    context "when validation fails" do
      it "should render form" do
        put "/relationships/#{relationship.id}", params: {
          relationship: { degree: "" }
        }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:form)
        relationship.reload
        expect(relationship.degree).not_to eq(new_degree)
      end
    end
    context "when user is not authorized to update owner" do
      before { unauthorize :update, relationship.owner }
      it "should not update relationship" do
        put "/relationships/#{relationship.id}", params: {
          relationship: { degree: new_degree }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
    context "when user is not authorized to update with" do
      before { unauthorize :update, relationship.with }
      it "should not update relationship" do
        put "/relationships/#{relationship.id}", params: {
          relationship: { degree: new_degree }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # DESTROY RELATIONSHIP
  describe "DELETE /relationships/:id" do
    let(:relationship) { FactoryBot.create(:relationship) }
    before do
      authorize :update, relationship.owner
      authorize :update, relationship.with
    end
    it "should delete relationship" do
      expect {
        delete "/relationships/#{relationship.id}"
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:destroy)
      }.to change(Relationship, :count).from(2).to(0)
    end
    context "when user is not authorized to update owner" do
      before { unauthorize :update, relationship.owner }
      it "should not delete relationship" do
        delete "/relationships/#{relationship.id}"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
    context "when user is not authorized to update with" do
      before { unauthorize :update, relationship.with }
      it "should not delete relationship" do
        delete "/relationships/#{relationship.id}"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end
end
