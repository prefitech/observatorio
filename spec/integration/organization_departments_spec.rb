describe "organization departments" do
  let(:user) { FactoryBot.create(:user, :confirmed) }

  before do
    sign_in(user)
    PaperTrail.request.whodunnit = user
  end

  # INDEX ORGANIZATION DEPARTMENT
  describe "GET /organization_departments" do
    let!(:organization) { FactoryBot.create(:organization) }
    let!(:organization_department1) do
      FactoryBot.create(:organization_department, organization: organization)
    end
    let!(:organization_department2) do
      FactoryBot.create(:organization_department, organization: organization)
    end
    let!(:organization_department3) do
      FactoryBot.create(:organization_department)
    end
    before { authorize :read, organization }
    it "should render departments of a organization" do
      get "/organization_departments", params: {
        organization_department: { organization_id: organization.id }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
      expect(response.body).to include(organization_department1.id)
      expect(response.body).to include(organization_department2.id)
      expect(response.body).not_to include(organization_department3.id)
    end
    it "should render departments of a organization in JSON" do
      get "/organization_departments.json", params: {
        organization_department: { organization_id: organization.id }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
    end
    context "when no organization given" do
      it "should respond bad request" do
        get "/organization_departments"
        expect(response).to have_http_status(:bad_request)
        expect(response).to render_template(:bad_request)
      end
    end
    context "when user is not authorized to read organization" do
      before { unauthorize :read, organization }
      it "should not render departments" do
        get "/organization_departments", params: {
          organization_department: { organization_id: organization.id }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # NEW ORGANIZATION DEPARTMENT FORM
  describe "GET /organization_departments/new" do
    let!(:organization) { FactoryBot.create(:organization) }
    before { authorize :update, organization }
    it "should render new organization department form" do
      get "/organization_departments/new", params: {
        organization_department: { organization_id: organization.id }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when no organization given" do
      it "should respond bad request" do
        get "/organization_departments/new"
        expect(response).to have_http_status(:bad_request)
        expect(response).to render_template(:bad_request)
      end
    end
    context "when user is not authorized to update organization" do
      before { unauthorize :update, organization }
      it "should not render form" do
        get "/organization_departments/new", params: {
          organization_department: { organization_id: organization.id }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # CREATE ORGANIZATION DEPARTMENT
  describe "POST /organization_departments" do
    let!(:organization) { FactoryBot.create(:organization) }
    let!(:name) { random_string }
    let!(:location) { FactoryBot.create(:location) }
    before { authorize :update, organization }
    it "should create organization_department" do
      expect {
        post "/organization_departments", params: {
          organization_department: {
            organization_id: organization.id,
            name: name,
            location_id: location.id
          }
        }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:create)
      }.to change(OrganizationDepartment, :count).from(0).to(1)
      created = OrganizationDepartment.last
      expect(created.organization).to eq(organization)
      expect(created.name).to eq(name)
      expect(created.location).to eq(location)
    end
    context "when parent_id is given" do
      let!(:parent) { FactoryBot.create(:organization_department) }
      before { authorize :update, parent }
      it "should create organization_department" do
        expect {
          post "/organization_departments", params: {
            organization_department: {
              name: name,
              parent_id: parent.id,
              location_id: location.id,
            }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:create)
        }.to change(OrganizationDepartment, :count).from(1).to(2)
        created = OrganizationDepartment.where(parent_id: parent.id).last
        expect(created.organization).to eq(parent.organization)
        expect(created.name).to eq(name)
        expect(created.location).to eq(location)
      end
    end
    context "when validation fails" do
      it "should not create organization_department" do
        expect {
          post "/organization_departments", params: {
            organization_department: {
              organization_id: organization.id,
              location_id: location.id
            }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:form)
        }.not_to change(OrganizationDepartment, :count)
      end
    end
    context "when user is not authorized to update organization" do
      before { unauthorize :update, organization }
      it "should not create organization_department" do
        expect {
          post "/organization_departments", params: {
            organization_department: {
              organization_id: organization.id,
              name: name,
              location_id: location.id
            }
          }
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(OrganizationDepartment, :count)
      end
    end
  end

  # SHOW ORGANIZATION DEPARTMENT
  describe "GET /organization_departments/:id" do
    let!(:organization) { FactoryBot.create(:organization) }
    let!(:organization_department) do
      FactoryBot.create(:organization_department, organization: organization)
    end
    before { authorize :read, organization }
    it "should render organization_department" do
      get "/organization_departments/#{organization_department.id}"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:show)
    end
    context "when user is not authorized to read organization" do
      before { unauthorize :read, organization }
      it "should not render organization_department" do
        get "/organization_departments/#{organization_department.id}"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # EDIT ORGANIZATION DEPARTMENT FORM
  describe "GET /organization_departments/:id/edit" do
    let!(:organization) { FactoryBot.create(:organization) }
    let!(:organization_department) do
      FactoryBot.create(:organization_department, organization: organization)
    end
    before { authorize :update, organization }
    it "should render edit organization_department form" do
      get "/organization_departments/#{organization_department.id}/edit"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when user is not authorized to update organization" do
      before { unauthorize :update, organization }
      it "should not render edit form" do
        get "/organization_departments/#{organization_department.id}/edit"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # UPDATE ORGANIZATION DEPARTMENT
  describe "PUT /organization_departments/:id" do
    let!(:organization) { FactoryBot.create(:organization) }
    let!(:organization_department) do
      FactoryBot.create(:organization_department, organization: organization)
    end
    let(:new_name) { random_string }
    let(:new_location) { FactoryBot.create(:location) }
    before { authorize :update, organization }
    it "should update organization_department" do
      put "/organization_departments/#{organization_department.id}", params: {
        organization_department: {
          name: new_name,
          location_id: new_location.id
        }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:update)
      organization_department.reload
      expect(organization_department.name).to eq(new_name)
      expect(organization_department.location).to eq(new_location)
    end
    context "when validation fails" do
      it "should not update organization_department" do
        expect {
          put "/organization_departments/#{organization_department.id}", params: {
            organization_department: {
              name: ""
            }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:form)
        }.not_to change(organization_department.reload, :name)
      end
    end
    context "when user is not authorized to update organization" do
      before { unauthorize :update, organization }
      it "should not update organization_department" do
        expect {
          put "/organization_departments/#{organization_department.id}", params: {
            organization_department: {
              name: new_name
            }
          }
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(organization_department.reload, :name)
      end
    end
  end

  # DESTROY ORGANIZATION DEPARTMENT
  describe "DELETE /organization_departments/:id" do
    let!(:organization) { FactoryBot.create(:organization) }
    let!(:organization_department) do
      FactoryBot.create(:organization_department, organization: organization)
    end
    before { authorize :update, organization }
    it "should delete organization_department" do
      expect {
        delete "/organization_departments/#{organization_department.id}"
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:destroy)
      }.to change(OrganizationDepartment, :count).from(1).to(0)
    end
  end
end
