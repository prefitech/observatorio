describe "organizations" do
  let(:user) { FactoryBot.create(:user, :confirmed) }

  before do
    sign_in(user)
    PaperTrail.request.whodunnit = user
  end

  # ORGANIZATIONS INDEX
  describe "GET /organizations" do
    let!(:suffix1) { "Facility" }
    let!(:suffix2) { "Nature" }

    let!(:organization1) { FactoryBot.create(:organization, name: "Organization A #{suffix1}") }
    let!(:organization2) { FactoryBot.create(:organization, :with_picture, name: "Organization B #{suffix1}") }
    let!(:organization3) { FactoryBot.create(:organization, name: "Organization C #{suffix2}") }
    let!(:organization4) { FactoryBot.create(:organization, name: "Organization D #{suffix2}") }
    let!(:organization5) { FactoryBot.create(:organization, name: "Organization E #{suffix2}") }
    let!(:organization6) { FactoryBot.create(:organization, name: "Organization F #{suffix2}") }
    let!(:organization7) { FactoryBot.create(:organization, name: "Organization G #{suffix2}") }
    let!(:organization8) { FactoryBot.create(:organization, name: "Organization H #{suffix2}") }
    let!(:organization9) { FactoryBot.create(:organization, name: "Organization I #{suffix2}") }
    let!(:organization10) { FactoryBot.create(:organization, name: "Organization J #{suffix2}") }
    let!(:organization11) { FactoryBot.create(:organization, name: "Organization K #{suffix2}") }
    let!(:organization12) { FactoryBot.create(:organization, name: "Organization L #{suffix2}") }

    before { authorize :list, Organization }
    it "should list only 8 organizations per page" do
      get "/organizations"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
      expect(response.body).to include(organization1.name)
      expect(response.body).to include(organization2.name)
      expect(response.body).to include(organization3.name)
      expect(response.body).to include(organization4.name)
      expect(response.body).to include(organization5.name)
      expect(response.body).to include(organization6.name)
      expect(response.body).to include(organization7.name)
      expect(response.body).to include(organization8.name)
      expect(response.body).not_to include(organization9.name)
      expect(response.body).not_to include(organization10.name)
      expect(response.body).not_to include(organization11.name)
      expect(response.body).not_to include(organization12.name)
    end
    it "should limit search results to 8 organizations per page" do
      get "/organizations", params: { search: suffix2 }
      expect(response.body).to include(organization3.name)
      expect(response.body).to include(organization4.name)
      expect(response.body).to include(organization5.name)
      expect(response.body).to include(organization6.name)
      expect(response.body).to include(organization7.name)
      expect(response.body).to include(organization8.name)
      expect(response.body).to include(organization9.name)
      expect(response.body).to include(organization10.name)
      expect(response.body).not_to include(organization11.name)
      expect(response.body).not_to include(organization12.name)
    end
    it "should only return 2 organizations" do
      get "/organizations", params: { search: suffix1 }
      expect(response.body).to include(organization1.name)
      expect(response.body).to include(organization2.name)
      expect(response.body).not_to include(organization3.name)
      expect(response.body).not_to include(organization4.name)
      expect(response.body).not_to include(organization5.name)
      expect(response.body).not_to include(organization6.name)
      expect(response.body).not_to include(organization7.name)
      expect(response.body).not_to include(organization8.name)
      expect(response.body).not_to include(organization9.name)
      expect(response.body).not_to include(organization10.name)
      expect(response.body).not_to include(organization11.name)
      expect(response.body).not_to include(organization12.name)
    end
    context "when user is not authorized to list organizations" do
      before { unauthorize :list, Organization }
      it "should not list organizations" do
        get "/organizations", params: { search: suffix1 }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # NEW ORGANIZATION FORM
  describe "GET /organizations/new" do
    before { authorize :create, Organization }
    it "should render new organization form" do
      get "/organizations/new"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when user is not authorized to create organizations" do
      before { unauthorize :create, Organization }
      it "should respond forbidden" do
        get "/organizations/new"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # CREATE ORGANIZATION
  describe "POST /organizations" do
    let(:tag1) { FactoryBot.create(:tag, :organization) }
    let(:tag2) { FactoryBot.create(:tag, :organization) }
    let(:name) { random_string }
    let(:cnpj) { "24.100.062/0001-33" }
    let(:headquarters) { FactoryBot.create(:location) }
    let(:date_of_foundation) { Date.yesterday }
    let(:date_of_extinction) { Date.today }
    before { authorize(:create, Organization) }
    it "should create organization" do
      expect {
        post "/organizations", params: {
          organization: {
            name: name,
            tag_ids: [tag1.id, tag2.id],
            headquarters_id: headquarters.id,
            date_of_foundation: date_of_foundation,
            date_of_extinction: date_of_extinction,
            approximate_dates: true,
            school: true,
            cnpj: cnpj
          }
        }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:create)
      }.to change(Organization, :count).from(0).to(1)
      created_organization = Organization.first
      expect(created_organization.name).to eq(name)
      expect(created_organization).to be_school
      expect(created_organization.headquarters).to eq(headquarters)
      expect(created_organization.tag_ids).to contain_exactly(tag1.id, tag2.id)
      expect(created_organization.date_of_foundation).to eq(date_of_foundation)
      expect(created_organization.date_of_extinction).to eq(date_of_extinction)
      expect(created_organization.formatted_cnpj).to eq(cnpj)
    end
    context "when validation fails" do
      it "should not create organization" do
        expect {
          post "/organizations", params: {
            organization: {
              name: ""
            }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:form)
        }.not_to change(Organization, :count)
      end
    end
    context "when user is not authorized to create organizations" do
      before { unauthorize(:create, Organization) }
      it "should not create organization" do
        expect {
          post "/organizations", params: {
            organization: {
              name: name
            }
          }
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(Organization, :count)
      end
    end
  end

  # SHOW ORGANIZATION
  describe "GET /organizations/:id" do
    let!(:organization) { FactoryBot.create(:organization, :school) }
    before { authorize(:read, organization) }
    it "should show organization" do
      get "/organizations/#{organization.id}"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:show)
    end
    context "when user is not authorized to read the organization" do
      before { unauthorize(:read, organization) }
      it "should not show organization" do
        get "/organizations/#{organization.id}"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # SHOW ORGANIZATION'S HISTORY
  describe "GET /organizations/:id/history" do
    let!(:organization) { FactoryBot.create(:organization) }
    before { authorize :read, organization }
    it "should show organizations's changes history" do
      get "/organizations/#{organization.id}/history"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:history)
    end
    context "when user is not authorized to read organization" do
      before { unauthorize :read, organization }
      it "should not show organizations's changes history" do
        get "/organizations/#{organization.id}/history"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # EDIT ORGANIZATION FORM
  describe "GET /organizations/:id/edit" do
    let(:organization) { FactoryBot.create(:organization) }
    before { authorize :update, organization }
    it "should render edit organization form" do
      get "/organizations/#{organization.id}/edit"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when user is not authorized to update the organization" do
      before { unauthorize :update, organization }
      it "should not render edit organization form" do
        get "/organizations/#{organization.id}/edit"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # UPDATE ORGANIZATION
  describe "PUT /organizations/:id" do
    let!(:tag) { FactoryBot.create(:tag, :organization) }
    let(:new_tag) { FactoryBot.create(:tag, :organization) }
    let!(:organization) { FactoryBot.create(:organization, tag_ids: [tag.id]) }
    let!(:name) { random_string }
    let!(:headquarters) { FactoryBot.create(:location) }
    before { authorize(:update, organization) }
    it "should update organization" do
      put "/organizations/#{organization.id}", params: {
        organization: {
          name: name,
          headquarters_id: headquarters.id,
          tag_ids: [new_tag.id]
        }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:update)
      organization.reload
      expect(organization.name).to eq(name)
      expect(organization.tag_ids).to contain_exactly(new_tag.id)
      expect(organization.headquarters).to eq(headquarters)
    end
    context "when validation fails" do
      it "should not update organization" do
        expect {
          put "/organizations/#{organization.id}", params: {
            organization: {
              name: ""
            }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:form)
        }.not_to change { organization.reload.name }
      end
    end
    context "when user is not authorized to update the organization" do
      before { unauthorize(:update, organization) }
      it "should not update organization" do
        expect {
          put "/organizations/#{organization.id}", params: {
            organization: {
              name: name
            }
          }
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change { organization.reload.name }
      end
    end
  end

  # READ ORGANIZATION'S DESCRIPTION
  describe "GET /organizations/:id/description" do
    let(:organization) { FactoryBot.create(:organization) }
    before { authorize(:read, organization) }
    it "should show organization's description" do
      get "/organizations/#{organization.id}/description"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:description)
    end
    context "when user is not authorized to update the organization" do
      before { unauthorize(:read, organization) }
      it "should not show organization's description" do
        get "/organizations/#{organization.id}/description"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # EDIT ORGANIZATION'S DESCRIPTION
  describe "GET /organizations/:id/description/edit" do
    let(:organization) { FactoryBot.create(:organization) }
    before { authorize(:update, organization) }
    it "should show organization's edit description form" do
      get "/organizations/#{organization.id}/description/edit"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form_description)
    end
    context "when user is not authorized to update the organization" do
      before { unauthorize(:update, organization) }
      it "should not show organization's edit description form" do
        get "/organizations/#{organization.id}/description/edit"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # UPDATE ORGANIZATION'S DESCRIPTION
  describe "PUT /organizations/:id/description" do
    let(:organization) { FactoryBot.create(:organization) }
    let(:new_description) { random_string }
    before { authorize(:update, organization) }
    it "should update description" do
      expect {
        put "/organizations/#{organization.id}/description", params: {
          organization: { description: new_description }
        }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:update_description)
      }.to change { organization.reload.description }.to(new_description)
    end
    context "when user is not authorized to update the organization" do
      before { unauthorize(:update, organization) }
      it "should not authorize" do
        expect {
          put "/organizations/#{organization.id}/description"
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change { organization.reload.description }
      end
    end
  end

  # DESTROY ORGANIZATION
  describe "DELETE /organization/:id" do
    let!(:organization) { FactoryBot.create(:organization) }
    before { authorize(:delete, organization) }
    it "should delete organization" do
      expect {
        delete "/organizations/#{organization.id}"
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:destroy)
      }.to change(Organization, :count).from(1).to(0)
    end
    context "when user is not authorized to delete the organization" do
      before { unauthorize(:delete, organization) }
      it "should not delete organization" do
        expect {
          delete "/organizations/#{organization.id}"
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(Organization, :count)
      end
    end
  end
end
