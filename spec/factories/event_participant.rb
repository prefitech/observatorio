FactoryBot.define do
  factory :event_participant do
    event { |o| o.association(:event) }
    participant { |o| o.association([:person, :organization].sample) }
    from_datetime { DateTime.now - rand(50..100).days }
    to_datetime { DateTime.now - rand(10..40).days }
    time_zone { ActiveSupport::TimeZone.all.map(&:tzinfo).map(&:name).sample }
    approximate_dates { [true, false].sample }
  end
end
