FactoryBot.define do
  factory :education_course do
    name { "Name-#{SecureRandom.uuid}" }
  end
end
