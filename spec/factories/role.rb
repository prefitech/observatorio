FactoryBot.define do
  factory :role do
    name { SecureRandom.hex }
  end
end
