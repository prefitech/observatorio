FactoryBot.define do
  factory :memberships_import do
    user
    file_unencrypted { File.new(File.join(Rails.root, "spec", "files", "memberships_import_ok.ods")) }
    file_extension { ".ods" }
  end
end
