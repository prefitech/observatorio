FactoryBot.define do
  factory :organization_owner do
    owner { |o| o.association([:person, :organization].sample) }
    organization { |o| o.association(:organization) }
    portion { rand(0.0..1.0) }
  end
end
