FactoryBot.define do
  factory :organizations_import do
    user
    file_unencrypted { File.new(File.join(Rails.root, "spec", "files", "organizations_import_ok.ods")) }
    file_extension { ".ods" }
  end
end
