FactoryBot.define do
  factory :location do
    osm_id { rand(100..5000) }
    osm_type { ["node", "way", "relation"].sample }
    lat { rand(37.0..39.0) }
    lng { rand(24.0..30.0) }
    address do
      { "house_number" => rand(100..300),
        "city_district" => "District #{SecureRandom.hex}",
        "street" => "Street #{SecureRandom.hex}",
        "city" => "City #{SecureRandom.hex}",
        "state" => "State of #{SecureRandom.hex}",
        "country" => "Country #{SecureRandom.hex}" }
    end
    geojson do
      { "type" => "Point",
        "data" => [rand(48.0..49.0).truncate(8), rand(19.0..39.0).truncate(8)] }
    end
  end
end
