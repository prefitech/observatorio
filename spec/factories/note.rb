FactoryBot.define do
  factory :note do
    author { |o| o.association(:user) }
    subject { |o| o.association([:person, :organization].sample) }
    body { SecureRandom.uuid }
  end
end
